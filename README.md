# miniRV32

[![riscof-pipeline](https://gitlab.com/hoffma/minirv32_riscof_test/badges/master/pipeline.svg)](https://gitlab.com/hoffma/minirv32_riscof_test/)

Minimalistic RISC-V core implemented in VHDL. Currently implementing the RV32i
instruction set. Heavily inspired by this implementation in verilog:

https://github.com/BrunoLevy/learn-fpga/blob/master/FemtoRV/RTL/PROCESSOR/femtorv32_quark.v

## The Core

For now the CPU will need 5 cycles for most instructions (except of branch). I
am not sure yet if I want to build a standard RISC-Pipeline yet or just make it
smaller. For now I'll keep it like that and try to improve increase $f_{max}$.

The processor itself has a very simple (von-neumann style) interface to interact with the system
bus:

```vhdl
clk         : in STD_LOGIC;
nreset      : in STD_LOGIC;
mem_addr    : out STD_LOGIC_VECTOR(31 downto 0);
mem_wdata   : out STD_LOGIC_VECTOR(31 downto 0);
mem_wen     : out STD_LOGIC_VECTOR(3 downto 0);
mem_rdata   : in STD_LOGIC_VECTOR(31 downto 0);
mem_strb    : out STD_LOGIC;
mem_ack     : in STD_LOGIC
```

If the CPU wants to access the system-bus then the mem_strb will be high until
an ACK is received. The operation is considered read if mem_wen is zero. Else
it is a write operation with byte-wide enable.\
TODO: Maybe consider splitting it up into instruction- and data-bus, this could
make the processor internals easier to handle.

## System Bus

Each peripheral on the system bus basically has the same interface as the
processor:

```vhdl
    clk   : in std_logic;
    rst   : in std_logic;
    -- actual system bus
    cs    : in std_logic;
    addr  : in std_logic_vector(31 downto 0);
    wen   : in std_logic_vector(3 downto 0);
    din   : in std_logic_vector(31 downto 0);
    dout  : out std_logic_vector(31 downto 0);
    ack   : out std_logic;
    -- hardware and other pins go here
    -- [....]
```

The signals should be self-explanatory. The chip-select (cs) signal goes high
until an ACK is received by the bus arbiter.

All available devices have to be declared inside
[DeviceConfiguration.vhd](./hdl/DeviceConfiguration.vhd) and then connected to
the bus inside the [soc.vhd](./hdl/soc.vhd).

## State of 2023

### September '23

Began rewriting and improving the core.
- Register file read wasn't clocked, but is now.
- Created a new SoC-File with very simple devices and a chip-select-generator.
  No more wishbone for now.
- Timing still has to be improved inside the processor
- Right now all instructions take 5 cycles, since the state machine is a mess.
  This could probably go down to 3 cycles per instruction for rv32i
- All rv32i instructions should be implemented, except of ECALL, EBREAK and
  FENCE
- Still no interrupts. Will fix the processor a bit more, do thoroughly testing
  on the instructions (maybe start some kind of verification?) and then look
  further
- For the software, a new lib directory was created to inhabit common functions
  and memory maps.
- Update 29th: Looks like RISCOF[^1]-Tests is fully working now. Atleast all rv32i
  tests pass for now


[^1]: RISCOF-Website - https://riscof.readthedocs.io/

Core is working again on the Arty-A7 with a simple SPI-Display showing fibonacci sequence:

![Fibonacci](docs/fib_help.mp4)

TODO:\
- Get riscof CI pipeline working
- Split up repositories to have a clean repository just for the core and one
  for everything else.

## Build RISC-V Toolchain 

These instructions were taken from these two sources:
- https://github.com/YosysHQ/picorv32#building-a-pure-rv32i-toolchain
- https://github.com/riscv-collab/riscv-gnu-toolchain

```
sudo mkdir /opt/riscv
sudo chown $USER /opt/riscv
# This takes around 6.6 GiB of space!
git clone https://github.com/riscv/riscv-gnu-toolchain 

cd riscv-gnu-toolchain
# check out the latest tag
git checkout 2022.11.18
git submodule update --init --recursive
mkdir build
cd build
../configure --prefix=/opt/riscv
make
make -j$(nproc)
```

Then include /opt/riscv/ into your PATH:
```
export PATH="/opt/riscv/bin:$PATH"
```

## Compile for rv32i

To compile for the RV32i ISA the following flags have to be given to the
compiler ( https://five-embeddev.com/toolchain/2019/06/26/gcc-targets/ ):
- -march=rv32g alias for imafd. This includes the integer instructions
- -mabi=ilp32 this makes sure to only use rv32i


More links:

- RISC-V Instruction Set Manual: https://riscv.org/wp-content/uploads/2019/12/riscv-spec-20191213.pdf
- RISC-V Assembly Language Programmer Manual: https://shakti.org.in/docs/risc-v-asm-manual.pdf
- PicoRV32: https://github.com/cliffordwolf/picorv32
