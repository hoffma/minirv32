library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.mem_init.all;

entity soc is
  Port ( 
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    led : out STD_LOGIC_VECTOR (7 downto 0);
    -- uart device
    txd : out STD_LOGIC;
    rxd : in STD_LOGIC
  );
end soc;

architecture behave of soc is
  signal mem_addr, mem_wdat, mem_rdat : std_logic_vector(31 downto 0);
  signal mem_wen : std_logic_vector(3 downto 0);
  signal mem_strb, mem_ack : std_logic;

  signal chip_select : std_logic_vector(7 downto 0);

  -- device signals
  signal ram_do : std_logic_vector(31 downto 0) := (others => '0');
  signal ram_ack : std_logic;
  signal ram_sel : std_logic;

  signal led_do : std_logic_vector(31 downto 0) := (others => '0');
  signal led_ack : std_logic;
  signal led_sel : std_logic;

  signal uart_do : std_logic_vector(31 downto 0) := (others => '0');
  signal uart_sel : std_logic;
  signal uart_ack : std_logic;

  signal chip_sel_idx : integer := 0;
  signal led_reg : std_logic_vector(7 downto 0);
begin
  led <= led_reg;

  ram_sel <= chip_select(0) and mem_strb;
  led_sel <= chip_select(1) and mem_strb;
  uart_sel <= chip_select(2) and mem_strb;

  with chip_select select mem_rdat <=
    ram_do    when "00000001",
    led_do    when "00000010",
    uart_do   when "00000100",
    (others => '0') when others;

  with chip_select select mem_ack <=
    ram_ack   when "00000001",
    led_ack   when "00000010",
    uart_ack  when "00000100",
    '0' when others;

  inst_csg : entity work.csg
  port map (
    addr => mem_addr,
    cs_lines => chip_select
  );

  -- decode chip select signal for data line multiplexer
  process(chip_select, mem_addr, mem_rdat)
  begin
    -- downto for priority selection if more than one select is set
    chip_sel_idx <= 0;
    for i in chip_select'length-1 downto 0 loop
      if chip_select(i) = '1' then
        chip_sel_idx <= i;
      end if;
      end loop;
  end process;

  inst_proc : entity work.minirv32 
  port map (
      clk => clk,
      rst => rst,
      mem_addr => mem_addr,
      mem_wdata => mem_wdat,
      mem_wen => mem_wen,
      mem_rdata => mem_rdat,
      mem_strb => mem_strb,
      mem_ack => mem_ack
  );

  inst_ram_d : entity work.ram_device
  generic map (
    MEM_SIZE_BYTES => 32768*2,
    MEM_INIT => MEM_INIT_CONST 
  )
  port map (
    clk => clk,
    rst => rst,
    cs => ram_sel,
    addr => mem_addr,
    wen => mem_wen,
    din => mem_wdat,
    dout => ram_do,
    ack => ram_ack
  );

  inst_led_d : entity work.led_device
  port map (
    clk => clk,
    rst => rst,
    cs => led_sel,
    addr => mem_addr,
    wen => mem_wen,
    din => mem_wdat,
    dout => led_do,
    ack => led_ack,

    led_hw => led_reg
  );

  inst_uart_d : entity work.uart_device
  port map (
    clk => clk,
    rst => rst,
    cs => uart_sel,
    addr => mem_addr,
    wen => mem_wen,
    din => mem_wdat,
    dout => uart_do,
    ack => uart_ack,

    txd => txd,
    rxd => rxd
  );
end behave;
