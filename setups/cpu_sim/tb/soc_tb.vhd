library ieee;
use ieee.std_Logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.finish;
use std.textio.all;

entity soc_tb is
end soc_tb;

architecture behave of soc_tb is
    signal clk, rst : std_logic := '0';
    signal rxd, txd : std_logic;

    signal led : std_logic_vector(7 downto 0);

    constant UART_BRD : std_logic_vector(31 downto 0) := x"000000d9";
    signal uart_we : std_logic;
    signal uart_ack : std_logic;
    signal uart_rdy : std_logic;
    signal uart_rxd, uart_txd : std_logic;
    signal uart_din : std_logic_vector(7 downto 0);
    signal uart_dout : std_logic_vector(7 downto 0);
    
begin

    clk <= not clk after 5 ns; -- 25 MHz
    rst <= '1', '0' after 50 ns;

    stim : process
      variable tmp_data : std_logic_vector(7 downto 0);
      variable tmp_data_in : std_logic_vector(7 downto 0);
    begin
      wait until rst = '0';
      wait until led = x"99";
      tmp_data := std_logic_vector(to_unsigned(character'pos('a'), tmp_data'length));
      tmp_data_in := tmp_data;
      report "Simulation started. Got LED = 0x" & to_hstring(led);

      -- loop
      --   uart_din <= tmp_data;
      --   uart_we <= '1';
      --   wait until rising_edge(clk);
      --   uart_we <= '0';
      --   wait until uart_ack = '1';
      --   wait until rising_edge(clk);

      --   wait until uart_rdy = '1';
      --   tmp_data_in := uart_dout;
      --   report "Received value: 0x" & to_hstring(tmp_data_in) severity note;
      --   wait until rising_edge(clk);

      --   if unsigned(tmp_data_in) >= to_unsigned(character'pos('d'), tmp_data_in'length) then
      --     report "Received 'd', simluation done!" severity note;
      --     exit;
      --   end if;

      --   tmp_data := std_logic_vector(unsigned(tmp_data_in) + 1);
      -- end loop;

      wait until led = x"03";
      report "Simulation finished. Got LED = 0x" & to_hstring(led);
      wait for 2 us;
      finish;
    end process;

    rxd <= uart_txd;
    uart_rxd <= txd;

    inst_soc : entity work.soc
    port map (
        clk => clk,
        rst => rst,
        led => led,
        rxd => rxd,
        txd => txd
    );

    inst_uart : entity work.uart
    port map (
      clk => clk,
      brd => UART_BRD,
      we => uart_we,
      ack => uart_ack,
      din => uart_din,
      rdy => uart_rdy,
      dout => uart_dout,
      txd => uart_txd,
      rxd => uart_rxd
    );

end behave;
