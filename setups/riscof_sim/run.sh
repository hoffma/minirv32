#!/bin/bash

SCRIPT_PATH="$(dirname $0)/$(basename $0)"

make clean
make
# ./minirv32_riscof_tb --stop-time=2ms --max-stack-alloc=0 --ieee-asserts=disable -gMEM_FILE=sw/main.hex
./minirv32_riscof_tb --stop-time=2ms --max-stack-alloc=0 --ieee-asserts=disable -gMEM_FILE=sw/main.hex --vcd=test.ghk
# ./minirv32_riscof_tb --stop-time=4ms --ieee-asserts=disable -gMEM_FILE=sw/main.hex --vcd=test.ghk
tr '[:upper:]' '[:lower:]' < minirv32-tmp.signature > DUT-minirv32.signature
