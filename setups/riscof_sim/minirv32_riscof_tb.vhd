-- #################################################################################################
-- # << minirv32-riscof - Testbench for running RISCOF >>                                           #
-- # ********************************************************************************************* #
-- # This testbench is adapted to my needs from the testbench for the neorv32 processor            #
-- # ********************************************************************************************* #
-- # BSD 3-Clause License                                                                          #
-- #                                                                                               #
-- # Copyright (c) 2023, Stephan Nolting. All rights reserved.                                     #
-- #                                                                                               #
-- # Redistribution and use in source and binary forms, with or without modification, are          #
-- # permitted provided that the following conditions are met:                                     #
-- #                                                                                               #
-- # 1. Redistributions of source code must retain the above copyright notice, this list of        #
-- #    conditions and the following disclaimer.                                                   #
-- #                                                                                               #
-- # 2. Redistributions in binary form must reproduce the above copyright notice, this list of     #
-- #    conditions and the following disclaimer in the documentation and/or other materials        #
-- #    provided with the distribution.                                                            #
-- #                                                                                               #
-- # 3. Neither the name of the copyright holder nor the names of its contributors may be used to  #
-- #    endorse or promote products derived from this software without specific prior written      #
-- #    permission.                                                                                #
-- #                                                                                               #
-- # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS   #
-- # OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF               #
-- # MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE    #
-- # COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     #
-- # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE #
-- # GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED    #
-- # AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING     #
-- # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  #
-- # OF THE POSSIBILITY OF SUCH DAMAGE.                                                            #
-- # ********************************************************************************************* #
-- # https://github.com/stnolting/neorv32-riscof                               (c) Stephan Nolting #
-- #################################################################################################

library std;
use std.textio.all;
use std.env.finish;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity minirv32_riscof_tb is
  generic (
    MEM_FILE : string;            -- memory initialization file
    MEM_SIZE : natural := 2*1024*1024 -- total memory size in bytes (64k words á 32 bit)
  );
end minirv32_riscof_tb;

architecture behave of minirv32_riscof_tb is
  constant mem_size_max_c : natural := 2*1024*1024;
  constant mem_size_c : natural := MEM_SIZE;

  type mem8_t is array(natural range<>) of bit_vector(7 downto 0);

  impure function mem8_init_f(file_name : string; num_bytes : natural; byte_sel : natural) return mem8_t is
    file     text_file   : text open read_mode is file_name;
    variable text_line_v : line;
    variable mem8_v      : mem8_t(0 to num_bytes-1);
    variable index_v     : natural;
    variable word_v      : bit_vector(31 downto 0);
  begin
    mem8_v  := (others => (others => '0')); -- initialize to all-zero
    index_v := 0;
    while (endfile(text_file) = false) and (index_v < num_bytes) loop
      readline(text_file, text_line_v);
      hread(text_line_v, word_v);
      case byte_sel is
        when 0      => mem8_v(index_v) := word_v(07 downto 00);
        when 1      => mem8_v(index_v) := word_v(15 downto 08);
        when 2      => mem8_v(index_v) := word_v(23 downto 16);
        when others => mem8_v(index_v) := word_v(31 downto 24);
      end case;
      index_v := index_v + 1;
    end loop;
    return mem8_v;
  end function mem8_init_f;

  signal clk, rst : std_logic := '0';

  type cpu_interface is record
    addr : std_logic_vector(31 downto 0);
    wdata : std_logic_vector(31 downto 0);
    rdata : std_logic_vector(31 downto 0);
    wen : std_logic_vector(3 downto 0);
    strb : std_logic;
    ack : std_logic;
  end record;

  signal s_cpu : cpu_interface;
begin

  clk <= not clk after 5 ns;
  rst <= '1', '0' after 100 ns;

  inst_minirv : entity work.minirv32
  port map(
    clk => clk,
    rst => rst,
    mem_addr => s_cpu.addr,
    mem_wdata => s_cpu.wdata,
    mem_wen => s_cpu.wen,
    mem_rdata => s_cpu.rdata,
    mem_strb => s_cpu.strb,
    mem_ack => s_cpu.ack
  );

  -- stim: process
  -- begin
  --   report "Hello World" severity note;
  --   wait for 5 us;
  --   report "Hello warning" severity warning;
  --   wait for 5 us;
  --   assert false report "TB: MEM_FILE=" & MEM_FILE & ", MEM_SIZE=" & integer'image(MEM_SIZE) severity warning;
  --   wait for 5 us;
  --   finish;
  -- end process;

  -- External Main Memory [rwx] - Constructed from four parallel byte-wide memories ---------
  -- -------------------------------------------------------------------------------------------
  ext_mem_rw: process(clk)
    variable mem8_b0_v : mem8_t(0 to (mem_size_c/4)-1) := mem8_init_f(MEM_FILE, mem_size_c/4, 0); -- byte[0]
    variable mem8_b1_v : mem8_t(0 to (mem_size_c/4)-1) := mem8_init_f(MEM_FILE, mem_size_c/4, 1); -- byte[1]
    variable mem8_b2_v : mem8_t(0 to (mem_size_c/4)-1) := mem8_init_f(MEM_FILE, mem_size_c/4, 2); -- byte[2]
    variable mem8_b3_v : mem8_t(0 to (mem_size_c/4)-1) := mem8_init_f(MEM_FILE, mem_size_c/4, 3); -- byte[3]

    variable addr: integer range 0 to (MEM_SIZE/4)-1;
  begin
    if rising_edge(clk) then
      -- report "s_cpu.addr: 0x" & to_hstring(s_cpu.addr(23 downto 0));
      addr := to_integer(unsigned(s_cpu.addr(23 downto 2)));
      s_cpu.ack   <= s_cpu.strb;
      s_cpu.rdata <= (others => '0');
      if (s_cpu.strb = '1') then
        if (s_cpu.wen /= "0000") then -- byte-wide write access
          if (s_cpu.wen(0) = '1') then mem8_b0_v(addr) := to_bitvector(s_cpu.wdata(07 downto 00)); end if;
          if (s_cpu.wen(1) = '1') then mem8_b1_v(addr) := to_bitvector(s_cpu.wdata(15 downto 08)); end if;
          if (s_cpu.wen(2) = '1') then mem8_b2_v(addr) := to_bitvector(s_cpu.wdata(23 downto 16)); end if;
          if (s_cpu.wen(3) = '1') then mem8_b3_v(addr) := to_bitvector(s_cpu.wdata(31 downto 24)); end if;
        else -- word-aligned read access
          s_cpu.rdata(07 downto 00) <= to_stdlogicvector(mem8_b0_v(addr));
          s_cpu.rdata(15 downto 08) <= to_stdlogicvector(mem8_b1_v(addr));
          s_cpu.rdata(23 downto 16) <= to_stdlogicvector(mem8_b2_v(addr));
          s_cpu.rdata(31 downto 24) <= to_stdlogicvector(mem8_b3_v(addr));
        end if;
      end if;
    end if;
  end process ext_mem_rw;

  -- read/write address --


  -- Simulation Triggers --------------------------------------------------------------------
  -- -------------------------------------------------------------------------------------------
  sim_triggers: process(clk)
  begin
    if rising_edge(clk) then
      if (s_cpu.strb = '1') and (s_cpu.wen /= "0000") and (s_cpu.addr = x"F0000000") then
        -- report "HELLO F0000000" severity warning;
        case s_cpu.wdata is
          when x"CAFECAFE" => -- end simulation
            assert false report "Finishing simulation." severity note;
            finish;
          when others =>
            assert false report "Simulation failed. Received: 0x" & to_hstring(s_cpu.wdata) severity error;
            finish;
        end case;
      end if;
    end if;
  end process sim_triggers;


  -- Signature Dump -------------------------------------------------------------------------
  -- -------------------------------------------------------------------------------------------
  signature_dump: process(clk)
    file     dump_file : text open write_mode is "minirv32-tmp.signature";
    variable line_v    : line;
    variable prev_strb : std_logic := '0';
  begin
    if rising_edge(clk) then
      if (s_cpu.strb = '1') and (prev_strb = '0') and (s_cpu.wen /= "0000") and (s_cpu.addr = x"F0000004") then
        -- report "Signature received: 0x" & to_hstring(s_cpu.wdata);
        -- for i in 7 downto 0 loop -- write 32-bit as 8x lowercase HEX chars
        --   write(line_v, to_hexchar_f(s_cpu.wdata(3+i*4 downto 0+i*4)));
        -- end loop;
        write(line_v, to_hstring(s_cpu.wdata), right, 8);
        writeline(dump_file, line_v);
      end if;
      prev_strb := s_cpu.strb;
    end if;
  end process signature_dump;
end behave;
