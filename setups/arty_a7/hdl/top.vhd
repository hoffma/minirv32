library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top is
    Port ( 
        CLK100MHZ : in STD_LOGIC;
        btn : in STD_LOGIC_VECTOR (3 downto 0);
        sw  : in STD_LOGIC_VECTOR (3 downto 0);
        led : out STD_LOGIC_VECTOR (3 downto 0);
        led_g : out STD_LOGIC_VECTOR (3 downto 0);
        uart_rxd_out : out STD_LOGIC;
        uart_txd_in : in STD_LOGIC
    );
end top;

architecture Behavioral of top is
    component clk_wiz_0
    port (
        clk_out1          : out    std_logic;
        clk_in1           : in     std_logic
    );
    end component;
    signal clk_in : std_logic;
    signal rst : std_logic := '0';
    signal led_out : std_logic_vector(7 downto 0);
begin

    led <= led_out(3 downto 0);
    led_g <= led_out(7 downto 4);
        
    inst_clk : clk_wiz_0
    port map (
        clk_in1 => CLK100MHZ,
        clk_out1 => clk_in
    );
    
    process(clk_in)
    begin
        if rising_edge(clk_in) then
            rst <= BTN(0);
        end if;
    end process;
    
    inst_soc : entity work.soc
    port map (
        clk => clk_in,
        rst => rst,
        txd => uart_rxd_out,
        rxd => uart_txd_in,
        led => led_out
        -- spi interface
        -- sclk => spi_sclk,
        -- ncs => spi_ncs,
        -- mosi => spi_mosi
    );
end Behavioral;
