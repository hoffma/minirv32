# original source:
# https://github.com/stnolting/neorv32-setups/blob/main/vivado/arty-a7-test-setup/create_project.tcl


# get path of this script; 
set script_path [file dirname [file normalize [info script]]]
set origin_dir $script_path
puts $origin_dir

set board "arty-a7-35"

set outputdir "[file normalize "$origin_dir/work"]"
file mkdir $outputdir

set files [glob -nocomplain "$outputdir/*"]
if {[llength $files] != 0} {
    puts "deleting contents of $outputdir"
    file delete -force {*}[glob -directory $outputdir *]; # clear folder contents
} else {
    puts "$outputdir is empty"
}

switch $board {
  "arty-a7-35" {
    set a7part "xc7a35ticsg324-1L"
    set a7prj miniRV32-sample-${board}
  }
}

# Create project
create_project -part $a7part $a7prj $outputdir

set_property board_part digilentinc.com:${board}:part0:1.0 [current_project]
set_property target_language VHDL [current_project]

# Set project properties
set obj [current_project]
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "part" -value "xc7a35ticsg324-1L" -objects $obj
set_property -name "revised_directory_structure" -value "1" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj

# waveform configuration
# add_files [glob $script_path/waveforms/*.wcfg]

# Actual soft-core source
add_files [glob $script_path/../../hdl/core/*.vhd]
# Other hardware
add_files [glob $script_path/../../hdl/*.vhd]

# add_files [glob $script_path/../../hdl/core/minirv32.vhd]
# add_files [glob $script_path/../../hdl/core/uart.vhd]
# # config file
# add_files [glob $script_path/../../hdl/DeviceConfiguration.vhd]
# # other peripherals
# add_files [glob $script_path/../../hdl/csg.vhd]
# add_files [glob $script_path/../../hdl/soc.vhd]
# add_files [glob $script_path/../../hdl/misc.vhd]
# add_files [glob $script_path/../../hdl/spi_phy.vhd]
# #device wrappers
# add_files [glob $script_path/../../hdl/ram_device.vhd]
# add_files [glob $script_path/../../hdl/uart_device.vhd]
# add_files [glob $script_path/../../hdl/led_device.vhd]
# add_files [glob $script_path/../../hdl/dummy_device.vhd]
# add_files [glob $script_path/../../hdl/spi_device.vhd]
# actual sources for this board
add_files [glob $script_path/hdl/*.vhd]
# add_files [glob $script_path/hdl/top.vhd]
# add_files [glob $script_path/hdl/mem_init.vhd]

set fileset_constraints [glob $script_path/*.xdc]

set fileset_sim [glob $script_path/../../sim/*.vhd]

## Constraints
add_files -fileset constrs_1 $fileset_constraints

## Simulation-only
add_files -fileset sim_1 $fileset_sim

set_property file_type {VHDL 2008} [get_files -filter {FILE_TYPE == VHDL}]

# generate all needed IP's
source $script_path/clk_wiz_0.tcl
upgrade_ip [get_ips -all]

synth_design -top top -name synth_1
# Run synthesis, implementation and bitstream generation
launch_runs impl_1 -to_step write_bitstream -jobs 4
wait_on_run impl_1

