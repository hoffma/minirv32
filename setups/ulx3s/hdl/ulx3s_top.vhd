library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ulx3s_top is
    Port ( clk_25mhz: in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (3 downto 0);
           led : out STD_LOGIC_VECTOR (7 downto 0);
           ftdi_rxd : out STD_LOGIC;
           ftdi_txd : in STD_LOGIC;
           -- wifi_gpio16 : out STD_LOGIC; -- Serial1 RX, RV32 TX
           -- wifi_gpio17 : in STD_LOGIC; -- Serial1 TX, RV32 RX
           gp : inout STD_LOGIC_VECTOR(7 downto 0);
           wifi_gpio0 : out STD_LOGIC);
end ulx3s_top;

architecture Behavioral of ulx3s_top is
    
    signal rst : std_logic := '0';
    signal led_out : std_logic_vector(7 downto 0);
    signal clocks : std_logic_vector(3 downto 0);

    signal rxd_i : std_logic;
    signal txd_o : std_logic;

    -- signal rxd, txd : std_logic;
begin
    wifi_gpio0 <= '1'; -- prevent ulx3s from reboot

    led <= led_out;
    gp(7 downto 3) <= (others => '0');

    clkgen_inst : entity work.ecp5pll
    generic map (
        in_Hz => natural(25.0e6),
        out0_Hz => natural(25.0e6), out0_tol_hz => 0
        -- out1_Hz => natural(100.0e6), out1_tol_hz => 0
    ) port map (
        clk_i => clk_25mhz,
        clk_o => clocks
    );

    process(clocks(0))
    begin
        if rising_edge(clocks(0)) then
            rst <= not BTN(0);
            rxd_i <= ftdi_rxd;
            ftdi_rxd <= txd_o;
        end if;
    end process;

    inst_soc : entity work.soc
    port map (
        clk => clocks(0),
        rst => rst,
        led => led_out,
        rxd => rxd_i,
        txd => txd_o 
        -- rxd => ftdi_txd,
        -- txd => ftdi_rxd
    );
end Behavioral;
