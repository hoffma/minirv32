#!/usr/bin/env python3
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

from sys import argv

binfile = argv[1]
nwords = int(argv[2])

with open(binfile, "rb") as f:
    bindata = f.read()

assert len(bindata) < 4*nwords
assert len(bindata) % 4 == 0

mem_data = []

print('library IEEE;')
print('use IEEE.STD_LOGIC_1164.ALL;')
print('')
print('use work.misc.all;')
print('')
print('package MEM_INIT is')
# print('    type mem32_t is array(natural range<>) of std_logic_vector(31 downto 0);')
# print('    type mem8_t is array(natural range<>) of std_logic_vector(7 downto 0);')
print("\tconstant MEM_INIT_CONST : mem32_t(0 to %d) := (" % (nwords-1))
for i in range(nwords):
    if i < len(bindata) // 4:
        w = bindata[4*i : 4*i+4]
        mem_data.append(w)
        print("\t\t\t\tx\"%02x%02x%02x%02x\"" % (w[3], w[2], w[1], w[0]), end='')
    else:
        # print("\t\t\t\tx\"%08x\"" % 0, end='')
        print("\t\t\t\tothers => (others => '0'));")
        break
    if i == nwords-1:
        print(");")
    else:
        print(",")

"""
for j in range(4):
    print("\tconstant MEM_INIT_CONST_B%d : mem8_t(0 to %d) := (" % (j, (nwords-1)))
    for i in range(nwords):
        if i < len(bindata) // 4:
            w = bindata[4*i : 4*i+4]
            print("\t\t\t\tx\"%02x\"" % (w[j]), end='')
        else:
            # print("\t\t\t\tx\"%08x\"" % 0, end='')
            print("\t\t\t\tothers => (others => '0'));")
            break
        if i == nwords-1:
            print(");")
        else:
            print(",")
"""
print('end MEM_INIT;')
