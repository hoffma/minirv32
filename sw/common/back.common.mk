# Mostly taken from: https://github.com/stnolting/neorv32/blob/main/sw/common/common.mk

APP_SRC ?= $(wildcard ./*.c) $(wildcard ./*.cpp) $(wildcard ./*.s)

APP_INC ?= -I .
ASM_INC ?= -I .

EFFORT ?= -O3

MARCH ?= rv32i
# MABI ?= ilp32
MABI = 

USER_FLAGS ?=

ifndef SW_HOME
@echo "SW_HOME not defined"
exit 1
endif

COM_PATH ?= $(SW_HOME)/common

LD_SCRIPT ?= $(COM_PATH)/sections.ld
STARTUP_SRC = $(COM_PATH)/start.s

ifndef PROJ_NAME
APP_IMG = mem_init.vhd
else
APP_IMG = $(PROJ_NAME)_init.vhd
endif
APP_ELF = main.elf
APP_BIN = main.bin

# Important to have startup first, so it gets linked correctly
SRC = $(STARTUP_SRC)
SRC += $(APP_SRC)

OBJ = $(SRC:%=%.o)

RISCV_PREFIX = riscv32-unknown-elf-
CC = 		$(RISCV_PREFIX)gcc
CXX = 		$(RISCV_PREFIX)g++
OBJDUMP = 	$(RISCV_PREFIX)objdump
OBJCOPY = 	$(RISCV_PREFIX)objcopy
SIZE = 		$(RISCV_PREFIX)size

GCC_WARNS =
GCC_WARNS += -Werror -Wall -Wextra -Wundef -Wpointer-arith 
GCC_WARNS += -Wshadow -Wcast-qual -Wcast-align -Wwrite-strings
GCC_WARNS += -Wredundant-decls -pedantic
GCC_WARNS += -Wstrict-prototypes -Wmissing-prototypes -Wconversion
CC_FLAGS = $(EFFORT) $(GCC_WARNS) -ffreestanding -nostdlib

STD_INC_PATH = $(SW_HOME)/lib
STD_PATH = $(SW_HOME)/lib
STD_NAME = ministd
LIBS = -L$(STD_PATH) -l$(STD_NAME)

image: $(APP_IMG)
elf: $(APP_ELF)

%.s.o: %.s
	@$(CC) -c $(CC_FLAGS) $< -o $@

%.c.o: %.c
	$(CC) -save-temps -c $(CC_FLAGS) -I$(STD_INC_PATH) $< -o $@


$(APP_ELF): $(OBJ)
	$(CC) -march=$(MARCH) -Wl,-Bstatic,-T,$(LD_SCRIPT),--strip-debug $(CC_FLAGS) $(OBJ) -o $@ -static $(LIBS)
	@echo "Memory utilization:"
	@$(SIZE) $(APP_ELF)

main.bin: $(APP_ELF)
	@$(OBJCOPY) -O binary $< /dev/stdout > $@

$(APP_IMG): main.bin
	python3 $(SW_HOME)/tools/makerom.py $^ 8192 > $@


clean:
	rm -f *.o *.bin *.elf
	rm -f *.c.s
	rm -f *.c.i
	rm -f $(COM_PATH)/*.o
