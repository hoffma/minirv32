# Mostly taken from: https://github.com/stnolting/neorv32/blob/main/sw/common/common.mk

RISCV_PREFIX = riscv32-unknown-elf-
CC = 		$(RISCV_PREFIX)gcc
CXX = 		$(RISCV_PREFIX)g++
OBJDUMP = 	$(RISCV_PREFIX)objdump
OBJCOPY = 	$(RISCV_PREFIX)objcopy
SIZE = 		$(RISCV_PREFIX)size

APP_SRC ?= $(wildcard ./*.c) $(wildcard ./*.cpp) $(wildcard ./*.s)

APP_INC ?= -I .
ASM_INC ?= -I .

EFFORT ?= -Os

MARCH ?= rv32i
MABI ?= ilp32

USER_FLAGS ?=

ifndef SW_HOME
@echo "SW_HOME not defined"
exit 1
endif

COM_PATH ?= $(SW_HOME)/common
LIB_PATH ?= $(SW_HOME)/lib

LD_SCRIPT ?= $(COM_PATH)/sections.ld
STARTUP_SRC = $(COM_PATH)/crt0.s

ifndef PROJ_NAME
APP_IMG = mem_init.vhd
else
APP_IMG = $(PROJ_NAME)_init.vhd
endif
APP_ELF = main.elf
APP_BIN = main.bin

STARTUP_OBJ ?= $(STARTUP_SRC:%=%.o)

LIB_SRC ?= $(wildcard $(LIB_PATH)/src/*.c) $(wildcard $(LIB_PATH)/src/*.cpp) $(wildcard $(LIB_PATH)/src/*.s) 
LIB_OBJ ?= $(LIB_SRC:%=%.o)
LIB_INC ?= -I$(LIB_PATH)/inc

APP_OBJ ?= $(APP_SRC:%=%.o)

OBJ ?= $(STARTUP_OBJ) $(LIB_OBJ) $(APP_OBJ)

CC_WARNS =
CC_WARNS += -Werror -Wall -Wextra -Wundef -Wpointer-arith 
# CC_WARNS += -Wshadow -Wcast-qual -Wcast-align -Wwrite-strings
CC_WARNS += -Wredundant-decls -pedantic
CC_WARNS += -Wstrict-prototypes -Wmissing-prototypes -Wconversion

CC_FLAGS = $(EFFORT) $(CC_WARNS) -ffreestanding -nostdlib
LD_FLAGS =
LIBS = -lgcc

# TODO: if start.s is replaced by correct crt0.s then this might not be needed
# CC_FLAGS = $(EFFORT) $(GCC_WARNS) -ffreestanding -nostdlib

image: $(APP_IMG)
elf: $(APP_ELF)

$(LIB_PATH)/src/%.c.o: $(LIB_PATH)/src/%.c
	$(CC) -c $(CC_FLAGS) $(LIB_INC) $< -o $@

%.s.o: %.s
	$(CC) -c $(CC_FLAGS) $< -o $@

%.c.o: %.c
	$(CC) -c $(CC_FLAGS) $(LIB_INC) $< -o $@


$(APP_ELF): $(LIB_OBJ) $(STARTUP_OBJ) $(OBJ)
	@echo "APP_OBJ: $(APP_OBJ)"
	@echo "STARTUP_OBJ: $(STARTUP_OBJ)"
	@echo "LIB_OBJ: $(LIB_OBJ)"
	@echo "objects: $(OBJ)"
	$(CC) -mabi=ilp32 -march=$(MARCH) -Wl,-Bstatic,-T,$(LD_SCRIPT),--strip-debug $(CC_FLAGS) $(LIB_INC) $(OBJ) -o $@ $(LIBS)
	@echo "Memory utilization:"
	@$(SIZE) $(APP_ELF)

main.bin: $(APP_ELF)
	@$(OBJCOPY) -O binary $< /dev/stdout > $@

$(APP_IMG): main.bin
	python3 $(SW_HOME)/tools/makerom.py $^ 8192 > $@
	python3 -m compiledb make


clean:
	rm -f *.o *.bin *.elf
	rm -f *.c.s
	rm -f *.c.i
	rm -f $(APP_IMG)
cleanall: clean
	rm -f compile_commands.json
	rm -f $(COM_PATH)/*.o
	rm -f $(LIB_PATH)/src/*.o
