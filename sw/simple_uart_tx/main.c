
#define F_CPU 25000000

#include <ministd.h>
#include <stdint.h>

int main(void);

int main() {
  char c = 'A';
  /* set baud rate divisor */
  UART_BRD_PORT = (F_CPU / UART_BAUD);

  LED_PORT = 0xc3;
  print("Hello, World!\n");

  while (c <= 'Z') {
    LED_PORT = (c % 10);
    putchar(c++);
    wait_cycles(50000);
  }
  putchar('\n');

  LED_PORT = 0xaa;
  while (1) {
    wait_cycles(10);
  }
  return 0;
}
