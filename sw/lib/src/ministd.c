#include "ministd.h"

void busy_wait() {
  wait_cycles(5);
}

void wait_cycles(uint32_t c) {
    for (volatile uint32_t i = 0; i < c; i++)
    ;
}

void print(char *s) {
  while(*s) {
    putchar(*s);
    s++;
  }
}

void putchar(char c) {
  if (c == '\n') {
    putchar('\r');
  }
  UART_TX_PORT = c;
}

