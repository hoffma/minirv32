#pragma once

#include <stdint.h>

#define XSTR(x) STR(x)
#define STR(x) #x

#ifndef F_CPU
#define F_CPU 25000000 /* 25 MHz default clock */
#pragma message "F_CPU is " XSTR(F_CPU) " Hz"
#endif

#ifndef UART_BAUD
#define UART_BAUD 115200
#pragma message "Default UART_BAUD is: " XSTR(UART_BAUD)
#endif

// memory mapped hardware addresses

#define LED_BASE    (0x80000000)
#define UART_BASE   (0x80000100)
#define SPI_BASE    (0x80000200)
#define DUMMY_BASE  (0x90000000)

// #define LED_BASE    ((volatile uint32_t *) 0x80000000)
// #define UART_BASE   ((volatile uint32_t *) 0x80000100)
// #define DUMMY_BASE  ((volatile uint32_t *) 0x90000000)

#define LED_PORT      (*(volatile uint32_t *)(LED_BASE))
#define UART_TX_PORT  (*(volatile uint32_t *)(UART_BASE + 0x0))
#define UART_RX_PORT  (*(volatile uint32_t *)(UART_BASE + 0x4))
#define UART_BRD_PORT (*(volatile uint32_t *)(UART_BASE + 0x8))
#define SPI_PORT_TX   (*(volatile uint32_t *)(SPI_BASE + 0x0))
#define SPI_PORT_RX   (*(volatile uint32_t *)(SPI_BASE + 0x4))
#define SPI_PORT_BUSY (*(volatile uint32_t *)(SPI_BASE + 0x8))
#define DUMMY_PORT    (*(volatile uint32_t *)(DUMMY_BASE))

// void busy_wait(uint32_t cycles);
void busy_wait(void);
void wait_cycles(uint32_t c);
void print(char *s);
void putchar(char c);
