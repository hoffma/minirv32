#include <ministd.h>
#include <stdint.h>

int main(void);
void short_delay(void);
void delay(void);

void delay() {
  for (volatile int i = 0; i < 100000; i++)
    ;
}

void short_delay() {
  for (volatile int i = 0; i < 5; i++)
    ;
}

int main() {
  /* set baud rate divisor */
  // UART_BRD_PORT = (F_CPU / UART_BAUD);

  short_delay();
  LED_PORT = 0xaa;
  short_delay();
  LED_PORT = 0xc3;
  short_delay();
  LED_PORT = 0x55;
  while (1) {
    short_delay();
    LED_PORT = 0x00;
  }
  return 0;
}
