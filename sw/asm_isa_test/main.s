.global _start
.globl rvtest_entry_point

li a0, 0x80000000
li a1, 0x24
sw a1, 0(a0)

loop:
j loop

# li x1, 0xbeef
# lui x2, 0xdead0
# or x3, x1, x2
# la x4, testvalue # load address of 'tmp' into x4
# sb x3, 0(x4)  # store x3[7:0] at MEM[x4]
# 
# lw x5, 0(x4)  # load MEM[x3] into x4
# 
# lw x3, testvalue

# clear registers
#addi x1, zero, 0
#addi x2, zero, 0
#
#la x1, testvalue # load address of testvalue into x1
#lw x2, 0(x1) # load testvalue into x2
#la x1, testvalue2 # load address of testvalue2 into x1
#lw x3, 0(x1) # load testvalue into x3
#slli x3, x3, 12
#or x3, x2, x3
#
#li x4, 4
#srl x3, x3, x4
#addi x4, x4, 3
#sll x3, x3, x4
#nop
#srai x3, x3, 7 # 0xff for the upper byte
#
#fail:
#j fail
#
#.data
#testvalue2: .word 0x56789
#testvalue: .word 0x123

# .global testvalue
# .global testvalue2
