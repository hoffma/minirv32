
#define F_CPU (25000000)
#define UART_BAUD (115200)

#include <stdint.h>
#include <ministd.h>

int main(void);

int main() {
    UART_BRD_PORT = F_CPU/UART_BAUD; // baud 115200 bei 25MHz

    char c = '\0';
    LED_PORT = 0x55;
    
    print("Hello, World!\n");

    while(1) {
        c = (char) UART_RX_PORT;
        LED_PORT = c;

        if (c == '\r') putchar('\n');
        else putchar(c);

        wait_cycles(5);
    }

    return 0;
}
