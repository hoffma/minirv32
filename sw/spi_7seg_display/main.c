
#define F_CPU 25000000

#include <ministd.h>
#include <stdint.h>

typedef enum {
  ZERO,
  ONE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  DASH,
  ALPHA_E,
  ALPHA_H,
  ALPHA_L,
  ALPHA_P,
  BLANK
} seg_font_t;

typedef enum {
  NOOP,
  DIG_0,
  DIG_1,
  DIG_2,
  DIG_3,
  DIG_4,
  DIG_5,
  DIG_6,
  DIG_7,
  DECODE_M,
  INTENSITY,
  SCAN_LIMIT,
  SHUTDOWN,
  DISPLAY_TEST = 0xf
} max7221_addr_t;

enum {
  ON,
  OFF
};

int main(void);
void display_write(uint32_t addr, uint32_t data);
void display_init(void);
void display_print_num(uint32_t n);
void draw_help(void);

void display_write(uint32_t addr, uint32_t data) {
  uint32_t tmp = 0;
  tmp = (addr << 0) | (data << 8);

  while (SPI_PORT_BUSY) {}
  SPI_PORT_TX = tmp;
}

void display_init() {
  display_write(SHUTDOWN, 0x1); /* normal operation */
  display_write(DECODE_M, 0xff); /* decode all digits */
  display_write(INTENSITY, 0x7); /* half intensity */
  display_write(SCAN_LIMIT, 0x7); /* display all digits */
  /* blank all digits */
  for (uint32_t i = DIG_0; i <= DIG_7; ++i) {
    display_write(i, BLANK);
  }
}

void draw_help() {
    display_write(DIG_7, ALPHA_H);
    display_write(DIG_6, ALPHA_E);
    display_write(DIG_5, ALPHA_L);
    display_write(DIG_4, ALPHA_P);
}

void display_print_num(uint32_t n) {
  display_write(DIG_0, n%10);
  n /= 10;
  display_write(DIG_1, n%10);
  n /= 10;
  display_write(DIG_2, n%10);
}

int main() {
  uint32_t i = 0;
  uint32_t a = 0;
  uint32_t b = 1;
  uint32_t tmp = 0;
  /* set baud rate divisor */
  UART_BRD_PORT = (F_CPU / UART_BAUD);

  display_init();
  wait_cycles(800000);
  print("Hello, World!\n");
  draw_help();

  // display_write(DIG_7, ALPHA_H);
  // wait_cycles(300000);
  // display_write(DIG_6, ALPHA_E);
  // wait_cycles(300000);
  // display_write(DIG_5, ALPHA_L);
  // wait_cycles(300000);
  // display_write(DIG_4, ALPHA_P);
  wait_cycles(300000);

  while(1) {
    i++;
    LED_PORT = i;
    display_print_num(b);
    tmp = a + b;
    a = b;
    b = tmp;
    wait_cycles(800000);

    if (b >= 1000) {
      i = 0;
      a = 0;
      b = 1;
    }
  }
  return 0;
}
