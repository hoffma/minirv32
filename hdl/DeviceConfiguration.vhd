library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package DeviceConfiguration is
  subtype mask_t is std_logic_vector(23 downto 0);

  constant CSG_MASK_256   : mask_t := x"ffff_ff";
  constant CSG_MASK_512   : mask_t := x"ffff_fe";
  constant CSG_MASK_1K    : mask_t := x"ffff_fc";
  constant CSG_MASK_2K    : mask_t := x"ffff_f8";

  constant CSG_MASK_4K    : mask_t := x"ffff_f0";
  constant CSG_MASK_8K    : mask_t := x"ffff_e0";
  constant CSG_MASK_16K   : mask_t := x"ffff_c0";
  constant CSG_MASK_32K   : mask_t := x"ffff_80";

  constant CSG_MASK_64K   : mask_t := x"ffff_00";
  constant CSG_MASK_128K  : mask_t := x"fffe_00";
  constant CSG_MASK_256K  : mask_t := x"fffc_00";
  constant CSG_MASK_512K  : mask_t := x"fff8_00";

  constant CSG_MASK_1M    : mask_t := x"fff0_00";
  constant CSG_MASK_2M    : mask_t := x"ffe0_00";
  constant CSG_MASK_4M    : mask_t := x"ffc0_00";
  constant CSG_MASK_8M    : mask_t := x"ff80_00";

  constant CSG_MASK_16M   : mask_t := x"ff00_00";
  constant CSG_MASK_32M   : mask_t := x"fe00_00";
  constant CSG_MASK_64M   : mask_t := x"fc00_00";
  constant CSG_MASK_128M  : mask_t := x"f800_00";

  constant CSG_MASK_256M  : mask_t := x"f000_00";
  constant CSG_MASK_512M  : mask_t := x"e000_00";
  constant CSG_MASK_1G    : mask_t := x"c000_00";
  constant CSG_MASK_2G    : mask_t := x"8000_00";
  constant CSG_MASK_4G    : mask_t := x"0000_00";

  constant IMEM_SIZE_BYTES : natural := 16#8000#;
  constant DEMEM_SIZE_BYTES : natural := 16#8000#;
  constant SRAM_SIZE_BYTES : natural := (IMEM_SIZE_BYTES + DEMEM_SIZE_BYTES);

  type device_t is record
    enable_csg : std_logic;
    base_addr : std_logic_vector(23 downto 0);
    base_addr_mask : mask_t;
  end record;

  constant CSG_INDEX_OFFSET : integer := 8;

  type csg_devices is array(0 to 7) of device_t;

  -- default entry to fill the array
  constant CSG_DEFAULT_OFF : device_t := ('0', x"0000_00", CSG_MASK_256);
  -- actual devices
  constant CSG_SRAM : device_t := ('1', x"0000_00", CSG_MASK_64K);
  constant CSG_LED : device_t := ('1', x"8000_00", CSG_MASK_256);
  constant CSG_UART : device_t := ('1', x"8000_01", CSG_MASK_256);
  -- constant CSG_SPI : device_t := ('1', x"8000_02", CSG_MASK_256);
  -- constant CSG_DUMMY : device_t := ('1', x"9000_00", CSG_MASK_256);

  -- array used for chip select generation
  constant CSG_ENTRIES : csg_devices := (
    CSG_SRAM,
    CSG_LED,
    CSG_UART,
    -- CSG_SPI,
    -- CSG_DUMMY,
    others => CSG_DEFAULT_OFF
  );
end DeviceConfiguration;
