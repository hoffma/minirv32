library ieee;
use ieee.std_logic_1164.all;

entity led_device is
  Port (
    clk   : in std_logic;
    rst   : in std_logic;
    cs    : in std_logic;
    addr  : in std_logic_vector(31 downto 0);
    wen   : in std_logic_vector(3 downto 0);
    din   : in std_logic_vector(31 downto 0);
    dout  : out std_logic_vector(31 downto 0);
    ack   : out std_logic;
    -- hardware output pins
    led_hw : out std_logic_vector(7 downto 0)
  );
end led_device;

architecture behave of led_device is
  signal led_reg : std_logic_vector(7 downto 0);
  signal led_din : std_logic_vector(7 downto 0);
  signal led_we : std_logic;
begin
  -- TODO: Maybe verify that the address is just the base address

  led_hw <= led_reg;
  
  led_we <= wen(0) when cs = '1' else '0';
  led_din <= din(7 downto 0) when cs = '1' else (others => '0');
  
  reg_proc : process(clk)
  begin
    if rising_edge(clk) then
        if rst = '1' then
            led_reg <= (others => '0');
            ack <= '0';
        else
            ack <= '0';
            if cs = '1' then
                if led_we = '1' then
                    led_reg <= led_din;
                end if;
                
                dout <= x"0000_00" & led_reg;
                ack <= '1';
            end if;
        end if;
    end if;
  end process;
end behave;
