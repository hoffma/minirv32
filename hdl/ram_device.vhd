library ieee;
use ieee.std_logic_1164.all;

library work;
use work.misc.all;

entity ram_device is
  Generic (
      MEM_SIZE_BYTES : natural; -- Byte
      MEM_INIT : mem32_t
  );
  Port (
    clk   : in std_logic;
    rst   : in std_logic;
    cs    : in std_logic;
    addr  : in std_logic_vector(31 downto 0);
    wen   : in std_logic_vector(3 downto 0);
    din   : in std_logic_vector(31 downto 0);
    dout  : out std_logic_vector(31 downto 0);
    ack   : out std_logic
  );
end ram_device;

architecture behave of ram_device is
  signal dout_i : std_logic_vector(31 downto 0);
begin
  -- dout <= dout_i when cs = '1' else (others => 'Z');
  dout <= dout_i;

  inst_sram : entity work.sram
  generic map (
    MEM_SIZE_BYTES => MEM_SIZE_BYTES,
    MEM_INIT => MEM_INIT
  )
  port map (
    clk => clk,
    en => cs,
    wren => wen,
    addr => addr,
    din => din,
    dout => dout_i
  );

  process(clk)
  begin
    if rising_edge(clk) then
      ack <= '0';
      if cs = '1' then
        ack <= '1';
      end if;
    end if;
  end process;
end behave;
