-- ############################################################################
-- ################ REGISTER FILE DESCRIPTION #################################
-- ############################################################################
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regfile is
    Port (
        clk     : in std_logic;
        r1_addr : in std_logic_vector(4 downto 0);
        r2_addr : in std_logic_vector(4 downto 0);
        wr_addr : in std_logic_vector(4 downto 0);
        wr_data : in std_logic_vector(31 downto 0);
        wr_en   : in std_Logic;
        r1_data : out std_logic_vector(31 downto 0);
        r2_data : out std_logic_vector(31 downto 0)
    );
end regfile;

architecture behave of regfile is
    type regfile_t is array(0 to 31) of std_logic_vector(31 downto 0);
    signal regfile_dat : regfile_t;
begin
    r1_data <= (others => '0') when unsigned(r1_addr) = 0 else regfile_dat(to_integer(unsigned(r1_addr)));
    r2_data <= (others => '0') when unsigned(r2_addr) = 0 else regfile_dat(to_integer(unsigned(r2_addr)));

    process(clk)
    begin
        if rising_edge(clk) then
            if wr_en = '1' then
                regfile_dat(to_integer(unsigned(wr_addr))) <= wr_data;
            end if;
        end if;
    end process;
end behave;

