library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package CONSTANTS is
    constant ALU_ADD  : std_logic_vector(7 downto 0) := x"00";
    constant ALU_SUB  : std_logic_vector(7 downto 0) := x"01";

    constant ALU_SLT  : std_logic_vector(7 downto 0) := x"10";
    constant ALU_SLTU : std_logic_vector(7 downto 0) := x"11";

    constant ALU_XOR  : std_logic_vector(7 downto 0) := x"20";
    constant ALU_OR   : std_logic_vector(7 downto 0) := x"21";
    constant ALU_AND  : std_logic_vector(7 downto 0) := x"22";

    constant ALU_SLL  : std_logic_vector(7 downto 0) := x"40";
    constant ALU_SRL  : std_logic_vector(7 downto 0) := x"41";
    constant ALU_SRA  : std_logic_vector(7 downto 0) := x"42";

    constant ALU_BEQ : std_logic_vector(7 downto 0) := x"80";
    constant ALU_BNE : std_logic_vector(7 downto 0) := x"81";
    constant ALU_BLT : std_logic_vector(7 downto 0) := x"84";
    constant ALU_BGE : std_logic_vector(7 downto 0) := x"85";
    constant ALU_BLTU : std_logic_vector(7 downto 0) := x"86";
    constant ALU_BGEU : std_logic_vector(7 downto 0) := x"87";
end package CONSTANTS;

package body CONSTANTS is

end package body CONSTANTS;
