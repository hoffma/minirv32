library ieee;
use ieee.std_logic_1164.all;

entity barrel_shifter is
    Port (
        din         : in std_logic_vector(31 downto 0);
        shamt       : in std_logic_vector(4 downto 0);
        shift_op    : in std_logic_vector(1 downto 0);

        dout        : out std_logic_vector(31 downto 0)
    );
end barrel_shifter;

architecture behave of barrel_shifter is
    signal shift1, shift2, shift4 : std_logic_vector(31 downto 0);
    signal shift8, shift16 : std_logic_vector(31 downto 0);

    constant ZERO_PAD : std_logic_vector(16 downto 0) := (others => '0');
begin

    with (shamt(0)&shift_op) select shift1 <=
        din(30 downto 0) & ZERO_PAD(0) when "100",
        ZERO_PAD(0) & din(31 downto 1) when "101",
        din(31) & din(31 downto 1) when "110",
        din when others;

    with (shamt(1)&shift_op) select shift2 <=
        shift1(29 downto 0) & ZERO_PAD(1 downto 0) when "100",
        ZERO_PAD(1 downto 0) & shift1(31 downto 2) when "101",
        (1 downto 0 => shift1(31)) & shift1(31 downto 2) when "110",
        shift1 when others;

    with (shamt(2)&shift_op) select shift4 <=
        shift2(27 downto 0) & ZERO_PAD(3 downto 0) when "100",
        ZERO_PAD(3 downto 0) & shift2(31 downto 4) when "101",
        (3 downto 0 => shift2(31)) & shift2(31 downto 4) when "110",
        shift2 when others;

    with (shamt(3)&shift_op) select shift8 <=
        shift4(23 downto 0) & ZERO_PAD(7 downto 0) when "100",
        ZERO_PAD(7 downto 0) & shift4(31 downto 8) when "101",
        (7 downto 0 => shift4(31)) & shift4(31 downto 8) when "110",
        shift4 when others;

    with (shamt(4)&shift_op) select shift16 <=
        shift8(15 downto 0) & ZERO_PAD(15 downto 0) when "100",
        ZERO_PAD(15 downto 0) & shift8(31 downto 16) when "101",
        (15 downto 0 => shift8(31)) & shift8(31 downto 16) when "110",
        shift8 when others;

    dout <= shift16;
end behave;
