-- ############################################################################
-- ######################## ALU DESCRIPTION ###################################
-- ############################################################################
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.constants.all;

entity alu is
    Port (
        a       : in std_logic_vector(31 downto 0);
        b       : in std_logic_vector(31 downto 0);
        alu_op  : in std_logic_vector(7 downto 0);

        y       : out std_logic_vector(31 downto 0);
        LT      : out std_logic;
        LTU     : out std_Logic;
        EQ      : out std_logic
    );
end alu;

architecture behave of alu is
    signal a_s, b_s : signed(31 downto 0);
    signal a_u, b_u : unsigned(31 downto 0);

    signal LT_o : std_logic;
    signal LTU_o : std_logic;
    signal EQ_o : std_logic;

    signal shamt : std_logic_vector(4 downto 0);
    signal shift_op : std_logic_vector(1 downto 0);
    signal shift_out : std_logic_vector(31 downto 0);
begin
    LT <= LT_o;
    LTU <= LTU_o;
    EQ  <= EQ_o;

    a_s <= signed(a);
    b_s <= signed(b);
    a_u <= unsigned(a);
    b_u <= unsigned(b);

    -- shamt <= b(24 downto 20);
    shamt <= b(4 downto 0);
    shift_op <= alu_op(1 downto 0);

    LT_o <= '1' when a_s < b_s else '0';
    LTU_o <= '1' when a_u < b_u else '0';
    EQ_o <= '1' when a_u = b_u else '0';



    -- process(a, b, alu_op, a_s, b_s, shift_out)
    process(all)
        variable tmp_y : std_logic_vector(31 downto 0);
    begin
        tmp_y := (others => '0');

        case alu_op is
            when ALU_ADD =>
                tmp_y := std_logic_vector(a_s + b_s);
            when ALU_SUB =>
                tmp_y := std_logic_vector(a_s - b_s);
            when ALU_SLT =>
                tmp_y(0) := LT_o;
            when ALU_SLTU =>
                tmp_y(0) := LTU_o;
            when ALU_XOR =>
                tmp_y := a xor b;
            when ALU_OR =>
                tmp_y := a or b;
            when ALU_AND =>
                tmp_y := a and b;
            when ALU_SLL | ALU_SRL | ALU_SRA =>
                tmp_y := shift_out;
            when others =>
                tmp_y := (others => '0');
        end case;

        y <= tmp_y;
    end process;

    inst_shifter : entity work.barrel_shifter
    port map (
        din => a,
        shamt => shamt,
        shift_op => shift_op,
        dout => shift_out
    );

end behave;
