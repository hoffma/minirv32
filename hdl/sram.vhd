library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.misc.all;

entity sram is
    Generic (
        MEM_SIZE_BYTES : natural; -- Byte
        MEM_INIT : mem32_t
    );
    Port (
        clk : in STD_LOGIC;
        en : in STD_LOGIC;
        wren : in STD_LOGIC_VECTOR (3 downto 0);
        addr : in STD_LOGIC_VECTOR (31 downto 0);
        din : in STD_LOGIC_VECTOR (31 downto 0);
        dout : out STD_LOGIC_VECTOR (31 downto 0));
end sram;

architecture Behavioral of sram is
    signal rdata : std_logic_vector(31 downto 0) := (others => '0');
    signal addr_i : unsigned(log2_f(MEM_SIZE_BYTES/4)-1 downto 0); -- fit to actual address width

    signal mem_ram : mem32_t(0 to (MEM_SIZE_BYTES/4)-1) := MEM_INIT;
begin

    addr_i <= unsigned(addr(log2_f((MEM_SIZE_BYTES/4))+1 downto 2)); -- cut address to actual width and word align
    dout <= rdata;
    
    process(clk)
    begin
        if rising_edge(clk) then
            if en = '1' then
                if wren(0) = '1' then
                    mem_ram(to_integer(addr_i))(7 downto 0) <= din(7 downto 0);
                end if;
                if wren(1) = '1' then
                    mem_ram(to_integer(addr_i))(15 downto 8) <= din(15 downto 8);
                end if;
                if wren(2) = '1' then
                    mem_ram(to_integer(addr_i))(23 downto 16) <= din(23 downto 16);
                end if;
                if wren(3) = '1' then
                    mem_ram(to_integer(addr_i))(31 downto 24) <= din(31 downto 24);
                end if;
                
                if wren = "0000" then
                    rdata <= mem_ram(to_integer(addr_i));
                else
                    rdata <= (others => '0');
                end if;
            end if;
        end if;
    end process;
end Behavioral;
