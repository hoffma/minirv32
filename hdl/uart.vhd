-- ######################################################################################
-- ###################################  UART TX #########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_tx is
    Port (
        clk : in STD_LOGIC;
        txd : out STD_LOGIC;
        brd : in STD_LOGIC_VECTOR(31 downto 0);
        we  : in STD_LOGIC;
        ack : out STD_LOGIC;
        din : in STD_LOGIC_VECTOR(7 downto 0)
    );
end uart_tx;

architecture Behave of uart_tx is
    signal tx_counter : unsigned(31 downto 0);
    signal tx_buf : std_logic_vector(7 downto 0);
    signal tx_bit_cnt : unsigned(7 downto 0) := x"00";
    
    signal  tx_state : std_logic_vector(1 downto 0) := "01";
    constant TX_WAIT : std_logic_vector(1 downto 0) := "01";
    constant TX_SEND : std_logic_vector(1 downto 0) := "10";
    constant TX_END  : std_logic_vector(1 downto 0) := "11";
begin

    process(clk)
    begin
        if rising_edge(clk) then
            tx_counter <= tx_counter + 1;
            ack <= '0';
            
            case tx_state is
                when TX_WAIT =>
                    txd <= '1';
                    if we = '1' then
                        txd <= '0'; -- start bit
                        tx_buf <= din;
                        tx_bit_cnt <= x"08";
                        tx_counter <= (others => '0');
                        tx_state <= TX_SEND;
                    end if;
                when TX_SEND =>
                    if tx_counter >= unsigned(brd) and tx_bit_cnt > 0 then
                        txd <= tx_buf(0);
                        tx_bit_cnt <= tx_bit_cnt - 1;
                        tx_buf <= '0' & tx_buf(7 downto 1); -- shift right
                        tx_counter <= (others => '0');
                    elsif tx_counter >= unsigned(brd) then
                        txd <= '1';
                        tx_counter <= (others => '0');
                        tx_state <= TX_END;
                    end if;
                when TX_END =>
                    txd <= '1';
                    if tx_counter >= unsigned(brd) then
                        ack <= '1';
                        tx_state <= TX_WAIT;
                    end if;
                when others =>
                    tx_state <= TX_WAIT;
                    tx_counter <= (others => '0');
                    ack <= '0';
            end case;
        end if;
    end process;

end Behave;

-- ######################################################################################
-- ###################################  UART RX #########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_rx is
    Port (
        clk : in STD_LOGIC;
        rxd : in STD_LOGIC;
        brd : in STD_LOGIC_VECTOR(31 downto 0);
        rdy : out STD_LOGIC; -- data ready
        dout : out STD_LOGIC_VECTOR(7 downto 0)
    );
end uart_rx;

architecture Behave of uart_rx is
    signal rx_counter : unsigned(31 downto 0) := (others => '0');
    signal rx_buf : std_logic_vector(7 downto 0) := (others => '0');
    signal rx_bit_cnt : unsigned(7 downto 0) := (others => '0');
    
--    signal rx_state : std_logic_vector(2 downto 0) := "001";
--    constant RX_WAIT : std_logic_vector(2 downto 0) := "001";
--    constant RX_START : std_logic_vector(2 downto 0) := "010";
--    constant RX_RECV : std_logic_vector(2 downto 0) := "100";
    
    type rx_state_t is (RX_WAIT, RX_START, RX_RECV);
    signal rx_state : rx_state_t;
begin

    process(clk)
    begin
        if rising_Edge(clk) then
            rx_counter <= rx_counter + 1;
            rdy <= '0';
            
            case rx_state is
                when RX_WAIT =>
                    rx_buf <= (others => '0');
                    if rxd = '0' then
                        rx_state <= RX_START;
                        rx_counter <= (others => '0');
                        rx_bit_cnt <= x"08";
                    end if;
                -- when RX_WAIT =>
                when RX_START =>
                    if rx_counter >= unsigned(brd)/2 then -- wait for a little
                        if rxd = '0' then -- check if still start bit
                            rx_state <= RX_RECV;
                            rx_counter <= (others => '0');
                        else -- error
                            rx_state <= RX_WAIT;
                        end if;
                    end if;
                -- RX_START
                when RX_RECV =>
                    if rx_counter >= unsigned(brd) and rx_bit_cnt > 0 then
                        rx_buf <= rxd & rx_buf(7 downto 1); -- shift in from left
                        rx_bit_cnt <= rx_bit_cnt - 1;
                        rx_counter <= (others => '0');
                    elsif rx_counter >= unsigned(brd) then
                        if rxd = '1' then -- stop bit
                            dout <= rx_buf;
                            rdy <= '1';
                            rx_state <= RX_WAIT;
                            rx_counter <= (others => '0');
                        end if;
                    end if;
                -- when RX_RECV =>
                when others =>
                    rx_state <= RX_WAIT;
                    rx_buf <= (others => '0');
                    rx_counter <= (others => '0');
            end case;
        end if;
    end process;

end Behave;

-- ######################################################################################
-- ###################################  UART TOP ########################################
-- ######################################################################################

library ieee;
use ieee.std_logic_1164.all;

entity uart is
  Port (
    clk : in std_logic;
    brd : in std_logic_vector(31 downto 0);
    -- tx
    we : in std_logic;
    ack : out std_logic;
    din : in std_logic_vector(7 downto 0);
    -- rx
    rdy : out std_logic;
    dout : out std_logic_vector(7 downto 0);
    -- hardware ports
    txd : out std_logic;
    rxd : in std_logic
  );
end uart;

architecture behavioral of uart is
begin
  inst_tx : entity work.uart_tx
  port map (
    clk => clk,
    txd => txd,
    brd => brd,
    we => we,
    ack => ack,
    din => din
  );

  inst_rx : entity work.uart_rx
  port map (
    clk => clk,
    rxd => rxd,
    brd => brd,
    rdy => rdy,
    dout => dout
  );
end behavioral;
