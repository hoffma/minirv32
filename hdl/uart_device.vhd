library ieee;
use ieee.std_logic_1164.all;

entity uart_device is
  Port (
    clk   : in std_logic;
    rst   : in std_logic;
    cs    : in std_logic;
    addr  : in std_logic_vector(31 downto 0);
    wen   : in std_logic_vector(3 downto 0);
    din   : in std_logic_vector(31 downto 0);
    dout  : out std_logic_vector(31 downto 0);
    ack   : out std_logic;
    -- hardware output pins
    txd   : out std_logic;
    rxd   : in std_logic
  );
end uart_device;

architecture behave of uart_device is
  constant BASE_NIBBLE_TX : std_logic_vector(7 downto 0) := x"00";
  constant BASE_NIBBLE_RX : std_logic_vector(7 downto 0) := x"04";
  constant BASE_NIBBLE_BRD : std_logic_vector(7 downto 0) := x"08";

  signal dout_i : std_logic_vector(31 downto 0);
  signal uart_brd : std_logic_vector(31 downto 0);
  
  -- rx
  signal rx_rdy : std_logic;
  signal rx_reg : std_logic_vector(7 downto 0);
  signal rx_dout : std_logic_vector(7 downto 0);
  signal rx_valid : std_logic;
  -- tx
  signal tx_we : std_logic;
  signal tx_ack : std_logic;
  signal tx_din : std_logic_vector(7 downto 0);
begin

  dout <= dout_i;
  tx_din <= din(7 downto 0);
  

  process(clk)
    variable addr_i_nibble : std_logic_vector(7 downto 0);
  begin
    
    if rising_edge(clk) then
        if rst = '1' then
            tx_we <= '0';
            rx_valid <= '0';
            rx_reg <= (others => '0');
         else
            ack <= '0';
            tx_we <= '0';
            dout_i <= (others => '0');
            addr_i_nibble := addr(7 downto 0);
            
            -- save the received data in a temporary register
            -- this gets overwritten (for now) if the register gets not read in time
            -- maybe some sort of FIFO-Buffer would be nice here
            if rx_rdy = '1' then
                rx_valid <= '1';
                rx_reg <= rx_dout;
            end if;
            
            if cs = '1' then
                case addr_i_nibble is
                    when BASE_NIBBLE_TX =>
                        if wen /= "0000" then
                            tx_we <= '1';
                        end if;
                        if tx_ack = '1' then
                            tx_we <= '0';
                            ack <= '1';
                        end if;
                    -- /BASE_NIBBLE_TX
                    
                    when BASE_NIBBLE_RX =>
                        if rx_valid = '1' then
                            rx_valid <= '0';
                            dout_i <= x"0000_00" & rx_reg;
                            ack <= '1';
                        end if;
                    -- /BASE_NIBBLE_RX
                    
                    when BASE_NIBBLE_BRD =>
                        if wen(0) = '1' then
                            uart_brd(7 downto 0) <= din(7 downto 0);
                        end if;
                        if wen(1) = '1' then
                            uart_brd(15 downto 8) <= din(15 downto 8);
                        end if;
                        if wen(2) = '1' then
                            uart_brd(23 downto 16) <= din(23 downto 16);
                        end if;
                        if wen(3) = '1' then
                            uart_brd(31 downto 24) <= din(31 downto 24);
                        end if;
                        
                        dout_i <= uart_brd;
                        ack <= '1';
                    -- /BASE_NIBBLE_BRD
                    
                    when others =>
                        -- do nothing on wrong address, no error handling yet
                        ack <= '1';
                        dout_i <= (others => '0');
                end case;
            end if;
         end if;
      
    end if;
  end process;

  inst_tx : entity work.uart_tx
  port map (
    clk => clk,
    txd => txd,
    brd => uart_brd,
    we => tx_we,
    ack => tx_ack,
    din => tx_din
  );

  inst_rx : entity work.uart_rx
  port map (
    clk => clk,
    rxd => rxd,
    brd => uart_brd,
    rdy => rx_rdy,
    dout => rx_dout
  );

end behave;
