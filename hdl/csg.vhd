library ieee;
use ieee.std_logic_1164.all;

library work;
use work.DeviceConfiguration.all;

entity csg is
  Port (
    addr : in STD_LOGIC_VECTOR (31 downto 0);
    cs_lines : out STD_LOGIC_VECTOR (7 downto 0)
  );
end csg;

architecture Behavioral of csg is
  signal raw_cs_signals : std_logic_vector(7 downto 0);
  
begin
	gen_cs_signals : process(addr)
		variable cmp_bit : std_logic;
	begin
	   raw_cs_signals <= (others => '0');
		for i in csg_devices'left to csg_devices'right loop
			cmp_bit := '1';			
			for j in 23 downto 0 loop
                -- !base_addr_mask ignores bits that are 0 in the mask
                -- base_addr and addr bits have to be the same (xnor)
				cmp_bit := cmp_bit and ((not CSG_ENTRIES(i).base_addr_mask(j)) or
					(addr(j + CSG_INDEX_OFFSET) xnor CSG_ENTRIES(i).base_addr(j)));
			end loop;
			raw_cs_signals(i) <= CMP_BIT;
		end loop;
	end process gen_cs_signals;

  filter_cs_signals : process(raw_cs_signals)
  begin
    for i in 0 to csg_devices'length-1 loop
      cs_lines(i) <= raw_cs_signals(i) and CSG_ENTRIES(i).enable_csg;
    end loop;
  end process;
end Behavioral;
