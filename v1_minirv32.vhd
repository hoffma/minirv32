-- This version kinda works for now.
-- Implements most RV32i instructinos, no FENCE, no ECALL, no CSR.
-- Passes riscof RISC-V compliance test.
-- ############################################################################
-- ######################## ALU DESCRIPTION ###################################
-- ############################################################################
library ieee;
use ieee.std_logic_1164.all;

entity barrel_shifter is
    Port (
        din         : in std_logic_vector(31 downto 0);
        shamt       : in std_logic_vector(4 downto 0);
        shift_op    : in std_logic_vector(1 downto 0);

        dout        : out std_logic_vector(31 downto 0)
    );
end barrel_shifter;

architecture behave of barrel_shifter is
    signal shift1, shift2, shift4 : std_logic_vector(31 downto 0);
    signal shift8, shift16 : std_logic_vector(31 downto 0);

    constant ZERO_PAD : std_logic_vector(16 downto 0) := (others => '0');
begin

    with (shamt(0)&shift_op) select shift1 <=
        din(30 downto 0) & ZERO_PAD(0) when "100",
        ZERO_PAD(0) & din(31 downto 1) when "101",
        din(31) & din(31 downto 1) when "110",
        din when others;

    with (shamt(1)&shift_op) select shift2 <=
        shift1(29 downto 0) & ZERO_PAD(1 downto 0) when "100",
        ZERO_PAD(1 downto 0) & shift1(31 downto 2) when "101",
        (1 downto 0 => shift1(31)) & shift1(31 downto 2) when "110",
        shift1 when others;

    with (shamt(2)&shift_op) select shift4 <=
        shift2(27 downto 0) & ZERO_PAD(3 downto 0) when "100",
        ZERO_PAD(3 downto 0) & shift2(31 downto 4) when "101",
        (3 downto 0 => shift2(31)) & shift2(31 downto 4) when "110",
        shift2 when others;

    with (shamt(3)&shift_op) select shift8 <=
        shift4(23 downto 0) & ZERO_PAD(7 downto 0) when "100",
        ZERO_PAD(7 downto 0) & shift4(31 downto 8) when "101",
        (7 downto 0 => shift4(31)) & shift4(31 downto 8) when "110",
        shift4 when others;

    with (shamt(4)&shift_op) select shift16 <=
        shift8(15 downto 0) & ZERO_PAD(15 downto 0) when "100",
        ZERO_PAD(15 downto 0) & shift8(31 downto 16) when "101",
        (15 downto 0 => shift8(31)) & shift8(31 downto 16) when "110",
        shift8 when others;

    dout <= shift16;
end behave;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.misc.all;

entity alu is
    Port (
        a       : in std_logic_vector(31 downto 0);
        b       : in std_logic_vector(31 downto 0);
        alu_op  : in std_logic_vector(7 downto 0);

        y       : out std_logic_vector(31 downto 0);
        LT      : out std_logic;
        LTU     : out std_Logic;
        EQ      : out std_logic
    );
end alu;

architecture behave of alu is
    signal a_s, b_s : signed(31 downto 0);
    signal a_u, b_u : unsigned(31 downto 0);

    signal LT_o : std_logic;
    signal LTU_o : std_logic;
    signal EQ_o : std_logic;

    signal shamt : std_logic_vector(4 downto 0);
    signal shift_op : std_logic_vector(1 downto 0);
    signal shift_out : std_logic_vector(31 downto 0);
begin
    LT <= LT_o;
    LTU <= LTU_o;
    EQ  <= EQ_o;

    a_s <= signed(a);
    b_s <= signed(b);
    a_u <= unsigned(a);
    b_u <= unsigned(b);

    -- shamt <= b(24 downto 20);
    shamt <= b(4 downto 0);
    shift_op <= alu_op(1 downto 0);

    LT_o <= '1' when a_s < b_s else '0';
    LTU_o <= '1' when a_u < b_u else '0';
    EQ_o <= '1' when a_u = b_u else '0';



    -- process(a, b, alu_op, a_s, b_s, shift_out)
    process(all)
        variable tmp_y : std_logic_vector(31 downto 0);
    begin
        tmp_y := (others => '0');

        case alu_op is
            when ALU_ADD =>
                tmp_y := std_logic_vector(a_s + b_s);
            when ALU_SUB =>
                tmp_y := std_logic_vector(a_s - b_s);
            when ALU_SLT =>
                tmp_y(0) := LT_o;
            when ALU_SLTU =>
                tmp_y(0) := LTU_o;
            when ALU_XOR =>
                tmp_y := a xor b;
            when ALU_OR =>
                tmp_y := a or b;
            when ALU_AND =>
                tmp_y := a and b;
            when ALU_SLL | ALU_SRL | ALU_SRA =>
                tmp_y := shift_out;
            when others =>
                tmp_y := (others => '0');
        end case;

        y <= tmp_y;
    end process;

    inst_shifter : entity work.barrel_shifter
    port map (
        din => a,
        shamt => shamt,
        shift_op => shift_op,
        dout => shift_out
    );

end behave;

-- ############################################################################
-- ################ REGISTER FILE DESCRIPTION #################################
-- ############################################################################
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity regfile is
    Port (
        clk     : in std_logic;
        r1_addr : in std_logic_vector(4 downto 0);
        r2_addr : in std_logic_vector(4 downto 0);
        wr_addr : in std_logic_vector(4 downto 0);
        wr_data : in std_logic_vector(31 downto 0);
        wr_en   : in std_Logic;
        r1_data : out std_logic_vector(31 downto 0);
        r2_data : out std_logic_vector(31 downto 0)
    );
end regfile;

architecture behave of regfile is
    type regfile_t is array(0 to 31) of std_logic_vector(31 downto 0);
    signal regfile_dat : regfile_t;
begin
    r1_data <= (others => '0') when unsigned(r1_addr) = 0 else regfile_dat(to_integer(unsigned(r1_addr)));
    r2_data <= (others => '0') when unsigned(r2_addr) = 0 else regfile_dat(to_integer(unsigned(r2_addr)));

    process(clk)
    begin
        if rising_edge(clk) then
            if wr_en = '1' then
                regfile_dat(to_integer(unsigned(wr_addr))) <= wr_data;
            end if;
        end if;
    end process;
end behave;

-- ############################################################################
-- ####################### PROCESSOR DESCRIPTION ##############################
-- ############################################################################

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.misc.all;

entity minirv32 is
    Port (
        clk         : in STD_LOGIC;
        rst         : in STD_LOGIC;
        mem_addr    : out STD_LOGIC_VECTOR(31 downto 0);
        mem_wdata   : out STD_LOGIC_VECTOR(31 downto 0);
        mem_wen     : out STD_LOGIC_VECTOR(3 downto 0);
        mem_rdata   : in STD_LOGIC_VECTOR(31 downto 0);
        mem_strb    : out STD_LOGIC;
        mem_ack     : in STD_LOGIC
    );
end minirv32;

architecture Behavioral of minirv32 is

-- #### INSTRUCTION DECODE #####
    signal instr    : std_logic_vector(31 downto 0);
    signal funct7   : std_logic_vector(31 downto 25);
    signal rs2_addr : std_logic_vector(24 downto 20);
    signal rs1_addr : std_logic_vector(19 downto 15);
    signal funct3   : std_logic_vector(14 downto 12);
    signal rd_addr  : std_logic_vector(11 downto 7);
    signal opcode   : std_logic_vector(4 downto 0);


    -- immediates for each format
    -- no need for unsigned immediate, because:
    -- i.e., the immediate is first sign-extended to XLEN bits then treated as an unsigned number
    -- p.18, risc-v spec
    signal immI : std_logic_vector(31 downto 0);
    signal immU : std_logic_vector(31 downto 0);
    signal immS : std_logic_vector(31 downto 0);
    signal immB : std_logic_vector(31 downto 0);
    signal immJ : std_logic_vector(31 downto 0);
    signal immediate : std_logic_vector(31 downto 0);
-- #############################

-- #### PROGRAM COUNTER SIGNALS ####
    signal pc : std_logic_vector(31 downto 0) := (others => '0');
    signal next_pc, pc_plus_4, pc_plus_immU : std_logic_vector(31 downto 0);
-- ################################

-- #### ALU SIGNALS ####
    signal alu_y_reg : std_logic_vector(31 downto 0);
    signal alu_lt, alu_ltu, alu_eq : std_logic;
-- #####################

-- #### REGISTER FILE SIGNALS ####
    -- signal regf_r1_addr, regf_r2_addr, regf_wr_addr : std_logic_vector(4 downto 0);
    signal regf_r1_data, regf_r2_data, regf_wr_data : std_logic_vector(31 downto 0);
    signal regf_wr_en : std_logic;
-- ###############################

-- #### LOAD/STORE SIGNALS ####
    signal load_data : std_logic_vector(31 downto 0) := (others => '0');
    signal load_halfword : std_logic_vector(15 downto 0);
    signal load_byte : std_logic_vector(7 downto 0);
    signal loadstore_addr : std_logic_vector(31 downto 0) := (others => '0');
    signal store_mask : std_logic_vector(3 downto 0);
-- ###############################

-- #### CONTROL SIGNALS ####
    signal data_addr : std_logic_vector(31 downto 0);

    signal isLUI : std_logic;
    signal isAUIPC : std_logic;
    signal isJAL : std_logic;
    signal isJALR : std_logic;
    signal isBranch : std_logic;
    signal isLoad : std_logic;
    signal isStore : std_logic;
    signal isALUimm : std_logic;
    signal isALUreg : std_logic;
    -- signal isFENCE : std_logic; -- not used yet
    -- signal isSystem : std_logic -- ECALL/EBREAK, not used yet

    signal isALU : std_logic;
    signal b_predicate : std_logic;
    signal wb_data : std_logic_vector(31 downto 0);
    signal mem_wdata_sel : std_logic_vector(31 downto 0);
-- #########################

    signal mask_select : std_logic_vector(4 downto 0);

-- #### STATE SIGNALS ####
-- using one-hot encoding
    -- type pipeline_t is (STATE_IF, STATE_ID, STATE_EX, STATE_MEM);
    -- signal pipeline_state : pipeline_t;
    signal pipeline_state : std_logic_vector(4 downto 0) := "00001";
    constant STATE_IF  : std_logic_vector(4 downto 0) := "00001";
    constant STATE_ID  : std_logic_vector(4 downto 0) := "00010";
    constant STATE_EX  : std_logic_vector(4 downto 0) := "00100";
    constant STATE_MEM : std_logic_vector(4 downto 0) := "01000";
    constant STATE_WB  : std_logic_vector(4 downto 0) := "10000";

    constant LUI_OP     : std_logic_vector(4 downto 0) := "01101";
    constant AUIPC_OP   : std_logic_vector(4 downto 0) := "00101";
    constant JAL_OP     : std_logic_vector(4 downto 0) := "11011";
    constant JALR_OP    : std_logic_vector(4 downto 0) := "11001";
    constant BRANCH_OP  : std_logic_vector(4 downto 0) := "11000";
    constant LOAD_OP    : std_logic_vector(4 downto 0) := "00000";
    constant STORE_OP   : std_logic_vector(4 downto 0) := "01000";
    constant IMM_OP     : std_logic_vector(4 downto 0) := "00100";
    constant REG_REG_OP : std_logic_vector(4 downto 0) := "01100";
    constant FENCE_OP   : std_logic_vector(4 downto 0) := "00011";
    constant SYSTEM_OP  : std_logic_vector(4 downto 0) := "11100";
begin

-- #### INSTRUCTION DECODING #######
    -- select immediate
    immI <= (31 downto 11 => instr(31)) & instr(30 downto 25) & instr(24 downto 21) & instr(20);
    immS <= (31 downto 11 => instr(31)) & instr(30 downto 25) & instr(11 downto 8) & instr(7);
    immB <= (31 downto 12 => instr(31)) & instr(7) & instr(30 downto 25) & instr(11 downto 8) & '0';
    immU <= instr(31) & instr(30 downto 20) & instr(19 downto 12) & (11 downto 0 => '0');
    immJ <= (31 downto 20 => instr(31)) & instr(19 downto 12) & instr(20) & instr(30 downto 25) & instr(24 downto 21) & '0';

    with opcode select immediate <= immU when LUI_OP, -- LUI
                                    immU when AUIPC_OP, -- AUIPC
                                    immJ when JAL_OP, -- JAL
                                    immI when JALR_OP, -- JALR
                                    immB when BRANCH_OP, -- Branch
                                    immI when LOAD_OP, -- Load
                                    immS when STORE_OP, -- Store
                                    immI when IMM_OP, -- ALUimm
                                    (others => '0') when others;

    wb_data <= immediate when isLUI = '1' else
                alu_y_reg when isALU = '1' else
                pc_plus_immU when isAUIPC = '1' else
                pc_plus_4 when isJAL = '1' or isJALR = '1' else
                load_data when isLoad = '1' else
                (others => '0');


    isLUI <= '1'    when opcode = LUI_OP else '0';
    isAUIPC <= '1'  when opcode = AUIPC_OP else '0';
    isJAL <= '1'    when opcode = JAL_OP else '0';
    isJALR <= '1'   when opcode = JALR_OP else '0';
    isBranch <= '1' when opcode = BRANCH_OP else '0';
    isLoad <= '1'   when opcode = LOAD_OP else '0';
    isStore <= '1'  when opcode = STORE_OP else '0';
    isALUimm <= '1' when opcode = IMM_OP else '0';
    isALUreg <= '1' when opcode = REG_REG_OP else '0';
    isALU <= isALUimm or isALUreg;
-- #################################

-- #### LOAD / STORE LOGIC #########
    loadstore_addr <= std_logic_vector(signed(regf_r1_data) + signed(immS)) when instr(5) = '1' else
                    std_logic_vector(signed(regf_r1_data) + signed(immI));

    with loadstore_addr(1) select load_halfword <= mem_rdata(31 downto 16) when  '1',
                                                    mem_rdata(15 downto 0) when others;

    with loadstore_addr(0) select load_byte <= load_halfword(15 downto 8) when '1',
                                                load_halfword(7 downto 0) when others;

    with funct3 select load_data <= (31 downto 8 => load_byte(7)) & load_byte when "000",           -- LB
                                    (31 downto 16 => load_halfword(15)) & load_halfword when "001", -- LH
                                    (31 downto 8 => '0') & load_byte when "100",                    -- LBU
                                    (31 downto 16 => '0') & load_halfword when "101",               -- LHU
                                    mem_rdata when "010",                                           -- LW
                                    (others => '0') when others;

    mask_select <= funct3 & loadstore_addr(1 downto 0);

    with mask_select select store_mask <=
      "0001" when "00000",
      "0010" when "00001",
      "0100" when "00010",
      "1000" when "00011",
      "0011" when "00100",
      "1100" when "00110",
      "1111" when "01000",
      "0000" when others; -- illegal combination

    process(store_mask, regf_r2_data)
    begin
      mem_wdata_sel <= (others => '0');
      case store_mask is
        when "0001" =>
          mem_wdata_sel(7 downto 0) <= regf_r2_data(7 downto 0);
        when "0010" =>
          mem_wdata_sel(15 downto 8) <= regf_r2_data(7 downto 0);
        when "0100" =>
          mem_wdata_sel(23 downto 16) <= regf_r2_data(7 downto 0);
        when "1000" =>
          mem_wdata_sel(31 downto 24) <= regf_r2_data(7 downto 0);
        when "0011" =>
          mem_wdata_sel(15 downto 0) <= regf_r2_data(15 downto 0);
        when "1100" =>
          mem_wdata_sel(31 downto 16) <= regf_r2_data(15 downto 0);
        when "1111" =>
          mem_wdata_sel <= regf_r2_data;
        when others =>
      end case;
    end process;

-- #################################

-- #### ALU LOGIC ####

    -- TODO: possible new inputs
    -- with opcode select alu_a <=
    --   regf_r1_data when REG_REG_OP | IMM_OP | LOAD_OP | STORE_OP,
    --   pc when BRANCH_OP | JAL_OP | JALR_OP | AUIPC_OP,
    --   (others => '0') when LUI_OP,
    --   (others => '0') when others;

    -- with opcode select alu_b <=
    --   regf_r2_data when REG_REG_OP,
    --   x"0000_0004" when JAL_OP | JALR_OP,
    --   immediate when others;



    pc_plus_4 <= std_logic_vector(signed(pc) + 4);
    pc_plus_immU <= std_logic_vector(signed(pc) + signed(immU));

     alu_decode_block : block is
        signal alu_a, alu_b : std_logic_vector(31 downto 0);
        signal alu_y : std_logic_vector(31 downto 0);
        signal alu_op : std_logic_vector(7 downto 0);
     begin
        alu_a <= regf_r1_data;
        alu_b <= immediate when isALUimm = '1' else regf_r2_data;

        inst_alu : entity work.alu
        port map (
            a => alu_a,
            b => alu_b,
            alu_op => alu_op,
            y => alu_y,
            LT => alu_lt,
            LTU => alu_ltu,
            EQ => alu_eq
        );

        alu_decode : process(opcode, funct3, funct7)
        begin
            alu_op <= (others => '0');
            case opcode is
            when REG_REG_OP | IMM_OP  =>
                case funct3 is
                    when "000" =>
                        if opcode = REG_REG_OP and funct7(30) = '1' then
                            alu_op <= ALU_SUB;
                        else
                            alu_op <= ALU_ADD;
                        end if;
                    when "001" =>
                        alu_op <= ALU_SLL;
                    when "010" =>
                        alu_op <= ALU_SLT;
                    when "011" =>
                        alu_op <= ALU_SLTU;
                    when "100" =>
                        alu_op <= ALU_XOR;
                    when "101" =>
                        if funct7(30) = '1' then
                            alu_op <= ALU_SRA;
                        else
                            alu_op <= ALU_SRL;
                        end if;
                    when "110" =>
                        alu_op <= ALU_OR;
                    when "111" =>
                        alu_op <= ALU_AND;
                    when others =>
                        alu_op <= ALU_ADD;
                end case;
            -- /when REG_REG_OP | IMM_OP

            when BRANCH_OP =>
                alu_op <= "10000" & funct3;
            -- /when BRANCH_OP
            when OTHERS =>
                alu_op <= ALU_ADD;
            end case;
        end process;

        process(clk)
         begin
            if rising_edge(clk) then
                if rst = '1' then
                    alu_y_reg <= (others => '0');
                else
                    alu_y_reg <= alu_y;
                end if;
            end if;
         end process;
     end block alu_decode_block;


-- ###################

-- #### REGISTER FILE IMPLEMENTATION ####
    inst_regf : entity work.regfile
    Port map (
        clk =>  clk,
        r1_addr => rs1_addr,
        r2_addr => rs2_addr,
        wr_addr => rd_addr,
        wr_data => regf_wr_data,
        wr_en => regf_wr_en,
        r1_data => regf_r1_data,
        r2_data => regf_r2_data
    );
-- ###############################

-- #### PROCESSOR STATE MACHINE ####

    next_pc <= std_logic_vector(signed(pc) + signed(immJ)) when isJAL = '1' else
                std_logic_vector(signed(regf_r1_data) + signed(immI)) when isJALR = '1' else
                std_logic_vector(signed(pc) + signed(immB)) when (isBranch = '1' and b_predicate = '1') else
                std_logic_vector(signed(pc) + 4);

    with funct3 select b_predicate <= alu_eq      when "000",
                                    not alu_eq  when "001",
                                    alu_lt      when "100",
                                    not alu_lt  when "101",
                                    alu_ltu     when "110",
                                    not alu_ltu when "111",
                                    '0'         when others;

    -- with loadstore_addr(1 downto 0) select load_mask <=

    data_addr <=  loadstore_addr when (isLoad = '1' or isStore = '1')
                  else (others => '0');

    with pipeline_state select mem_addr <=
        data_addr when STATE_MEM|STATE_WB,
        pc when others;

    -- MOORE AUTOMATA:
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                instr <= (others => '0');
                funct7 <= (others => '0');
                rs1_addr <= (others => '0');
                rs2_addr <= (others => '0');
                funct3 <= (others => '0');
                rd_addr <= (others => '0');
                opcode <= (others => '0');
                pipeline_state <= STATE_IF;
                pc <= (others => '0');
                mem_wdata <= (others => '0');
                mem_wen <= (others => '0');
                regf_wr_data <= (others => '0');
                regf_wr_en <= '0';
                mem_strb <= '1';
            else
                mem_wen <= (others => '0');
                mem_strb <= '0';
                regf_wr_en <= '0';

                case pipeline_state is
                    when STATE_IF =>
                      mem_strb <= '1';
                      pipeline_state <= STATE_ID;
                    -- STATE_IF

                    when STATE_ID =>
                      if mem_ack = '1' then
                        instr       <= mem_rdata;
                        funct7      <= mem_rdata(31 downto 25);
                        rs2_addr    <= mem_rdata(24 downto 20);
                        rs1_addr    <= mem_rdata(19 downto 15);
                        funct3      <= mem_rdata(14 downto 12);
                        rd_addr     <= mem_rdata(11 downto 7);
                        opcode      <= mem_rdata(6 downto 2);

                        pipeline_state <= STATE_EX;
                      end if;
                    -- STATE_ID

                    when STATE_EX =>
                        mem_wdata <= mem_wdata_sel;
                        pipeline_state <=  STATE_MEM;
                        if isLoad = '1' then
                            mem_strb <= '1';
                            mem_wen <= (others => '0');
                        elsif isStore = '1' then
                            mem_strb <= '1';
                            mem_wen <= store_mask;
                        end if;
                    -- STATE_EX

                    when STATE_MEM =>
                        -- on state transition
                        -- regf_wr_en <= '1' when (not (isStore or isBranch)) = '1' else '0';
                        regf_wr_data <= wb_data;

                        if isBranch = '1' then
                            pc <= next_pc;
                            mem_strb <= '1';
                            pipeline_state <= STATE_IF;
                        elsif isLoad = '1' then
                            mem_strb <= '1';
                            if mem_ack = '1' then -- prefetch next instruction
                                regf_wr_en <= '1';
                                mem_strb <= '0';
                                pipeline_state <= STATE_WB;
                            end if;
                        elsif isStore = '1' then
                            mem_strb <= '1';
                            if mem_ack = '1' then -- prefetch next instruction
                                pipeline_state <= STATE_IF;
                                mem_strb <= '1';
                                pc <= next_pc;
                            end if;
                        else
                            -- pc <= next_pc;
                            -- mem_strb <= '1';
                            regf_wr_en <= '1';
                            pipeline_state <= STATE_WB;
                        end if;
                    -- STATE_MEM

                    when STATE_WB =>
                        mem_strb <= '1';
                        pc <= next_pc;
                        pipeline_state <= STATE_IF;

                    when others =>
                        pipeline_state <= STATE_IF;
                end case;
            end if;
        end if;
    end process;
-- #################################
end Behavioral;

-- ############################################################################
-- ####################### PROCESSOR WISHBONE WRAPPER #########################
-- ############################################################################

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity minirv32_wb is
    Port (
        wb_clk_i   : in STD_LOGIC;
        wb_rst_i   : in STD_LOGIC;
        -- master interface
        wbm_adr_o   : out STD_LOGIC_VECTOR(31 downto 0);
        wbm_dat_o   : out STD_LOGIC_VECTOR(31 downto 0);
        wbm_dat_i   : in STD_LOGIC_VECTOR(31 downto 0);
        wbm_we_o    : out STD_LOGIC;
        wbm_sel_o   : out STD_LOGIC_VECTOR(3 downto 0);
        wbm_stb_o   : out STD_LOGIC;
        wbm_ack_i   : in STD_LOGIC;
        wbm_cyc_o   : out STD_LOGIC
    );
end minirv32_wb;

architecture Behave of minirv32_wb is
    signal clk, rst : std_logic;
    signal mem_addr : std_logic_vector(31 downto 0);
    signal mem_rdat, mem_wdat : std_Logic_vector(31 downto 0);
    signal mem_wen : std_logic_vector(3 downto 0);
    signal mem_strb, mem_ack : std_logic;

    signal we : std_logic;

    -- TODO: change state machine to use enum types
    -- type state_t is (S_WAIT, S_START, S_ADDR, S_WRITE, S_READ);
    signal state : std_logic_vector(2 downto 0) := "001";
    constant IDLE : std_logic_vector(2 downto 0) := "001";
    constant WBSTART : std_logic_vector(2 downto 0) := "010";
    constant WBEND : std_logic_vector(2 downto 0) := "100";
begin
    clk <= wb_clk_i;
    rst <= wb_rst_i; -- wishbone reset is active high

    we <= mem_wen(0) or mem_wen(1) or mem_wen(2) or mem_wen(3);

    process(wb_clk_i)
    begin
        if rising_edge(wb_clk_i) then
            if wb_rst_i = '1' then
                wbm_adr_o <= (others => '0');
                wbm_dat_o <= (others => '0');
                wbm_we_o <= '0';
                wbm_sel_o <= (others => '0');
                wbm_stb_o <= '0';
                wbm_cyc_o <= '0';
                state <= IDLE;
            else
                case state is
                    when IDLE =>
                        if mem_strb = '1' then
                            wbm_adr_o <= mem_addr;
                            wbm_dat_o <= mem_wdat;
                            wbm_we_o <= we;
                            wbm_sel_o <= mem_wen;

                            wbm_stb_o <= '1';
                            wbm_cyc_o <= '1';
                            state <= WBSTART;
                        else
                            mem_ack <= '0';

                            wbm_stb_o <= '0';
                            wbm_cyc_o <= '0';
                            wbm_we_o <= '0';
                        end if;
                    -- IDLE

                    when WBSTART =>
                        if wbm_ack_i = '1' then
                            mem_rdat <= wbm_dat_i;
                            mem_ack <= '1';

                            state <= WBEND;

                            wbm_stb_o <= '0';
                            wbm_cyc_o <= '0';
                            wbm_we_o <= '0';
                        end if;
                    -- WBSTART

                    when WBEND =>
                        mem_ack <= '0';

                        state <= IDLE;
                    -- WBEND

                    when others =>
                        state <= IDLE;
                end case;
            end if;
        end if;
    end process;

    inst_proc : entity work.minirv32
    port map (
        clk => clk,
        rst => rst,
        mem_addr => mem_addr,
        mem_wdata => mem_wdat,
        mem_wen => mem_wen,
        mem_rdata => mem_rdat,
        mem_strb => mem_strb,
        mem_ack => mem_ack
    );
end Behave;
