----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/02/2022 03:43:43 AM
-- Design Name: 
-- Module Name: regfile_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use std.env.stop;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity regfile_tb is
--  Port ( );
end regfile_tb;

architecture behave of regfile_tb is
    signal clk : std_Logic := '0';
    signal r1a, r2a, wra : std_logic_vector(4 downto 0) := (others => '0');
    signal r1d, r2d, wrd : std_logic_vector(31 downto 0) := (others => '0');
    signal wren : std_logic;
begin

    clk <= not clk after 5 ns;

    process
    begin
        wren <= '0';
        r1a <= (others => '0');
        r2a <= (others => '0');
        for I in 0 to 31 loop
            wren <= '1';
            wra <= std_logic_vector(to_unsigned(I, wra'length));
            wrd <= std_logic_vector(to_unsigned(I, 32) + 1);
            wait until rising_edge(clk);
        end loop;

        wren <= '0';
        wra <= (others => '0');
        wrd <= (others => '0');
        wait until rising_edge(clk);

        for I in 0 to 15 loop
            r1a <= '0' & std_logic_vector(to_unsigned(I, 4));
            r2a <= '1' & std_logic_vector(to_unsigned(I, 4));
            wait until rising_edge(clk);
        end loop;

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);

        report "Finished simulation" severity note;
        stop;
    end process;

    inst_reg : entity work.regfile 
    port map (
        clk => clk,
        r1_addr => r1a,
        r2_addr => r2a,
        wr_addr => wra,
        wr_data => wrd,
        wr_en => wren,
        r1_data => r1d,
        r2_data => r2d
    );
end behave;
