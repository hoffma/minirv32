library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library std;
use std.env.stop;

entity soc_tb is
--  Port ( );
end soc_tb;

architecture Behavioral of soc_tb is
    signal clk, rst : std_logic := '0';
    signal txd, rxd : std_Logic;
    signal led : std_logic_vector(7 downto 0);
begin

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 25 ns;

    rxd <= '1'; -- default rxd is high
    
    stim: process
    begin
      wait until rst = '0';
      report "Simulation started";
      wait until led = x"01";
      report "Received LED = 0x01";
      wait until led = x"05";
      report "Received LED = 0x05";
      wait for 2 us;
      report "Simulation successfully finished";
      stop;
    end process;
    
    inst_soc : entity work.soc
    port map (
        clk => clk,
        rst => rst,
        txd => txd,
        rxd => rxd,
        led => led
    );
end Behavioral;
