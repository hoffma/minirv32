----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/10/2022 10:28:14 PM
-- Design Name: 
-- Module Name: uart_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use std.env.stop;

entity uart_tb is
--  Port ( );
end uart_tb;

architecture Behavioral of uart_tb is
    signal clk, txd_to_rxd : std_Logic := '0';
    
    signal tx_we, tx_ack : std_logic := '0';
    signal rx_rdy : std_logic := '0';
    
    signal brd : std_logic_vector(31 downto 0);
    signal din : std_logic_vector(7 downto 0);
    signal dout : std_logic_vector(7 downto 0);

    file file_INPUT : text;
begin
    clk <= not clk after 5 ns; -- 100 MHz;
    -- reg_div_din <= x"0000000a";
    brd <= x"00000364"; -- 115200
    
    process
        variable v_ILINE : line;
        variable v_DATA : std_logic_vector(7 downto 0);
    begin
        
        report "Start reding from file and sending with UART";
        file_open(file_INPUT, "/home/user/Desktop/uart_data.txt", read_mode);
        
        while not endfile(file_INPUT) loop
            readline(file_INPUT, v_ILINE);
            hread(v_ILINE, v_DATA);
            
            din <= v_DATA;
            tx_we <= '1';
            wait until rising_edge(clk);
            tx_we <= '0';
            
            wait until tx_ack = '1';
            wait until rising_edge(clk);
            wait for 1 us;
            
        end loop;
        file_close(file_INPUT);
        wait for 10 us;
        report "simulation finished";
        
         stop;    
    end process;
    
    inst_tx : entity work.uart_tx
    port map (
        clk => clk,
        txd => txd_to_rxd,
        brd => brd,
        we => tx_we,
        ack => tx_ack,
        din => din
    );
    
    inst_rx : entity work.uart_rx
    port map (
        clk => clk,
        rxd => txd_to_rxd,
        brd => brd,
        rdy => rx_rdy,
        dout => dout
    );

end Behavioral;
