library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.stop;

entity uart_device_tb is
end uart_device_tb;

architecture behave of uart_device_tb is
  signal clk, rst : std_logic := '0';
  -- uart_device signals
  signal ud1_cs : std_logic := '0';
  signal ud1_ack : std_logic;
  signal ud1_wen : std_logic_vector(3 downto 0);
  signal ud1_addr : std_logic_vector(31 downto 0);
  signal ud1_din : std_logic_vector(31 downto 0);
  signal ud1_dout : std_logic_vector(31 downto 0);

  signal ud2_cs : std_logic := '0';
  signal ud2_ack : std_logic;
  signal ud2_wen : std_logic_vector(3 downto 0);
  signal ud2_addr : std_logic_vector(31 downto 0);
  signal ud2_din : std_logic_vector(31 downto 0);
  signal ud2_dout : std_logic_vector(31 downto 0);

  signal rxd, txd : std_logic;

  signal out_reg : std_logic_vector(31 downto 0);

  constant UART_TX_ADDR : std_logic_vector(31 downto 0) := x"8000_0100";
  constant UART_RX_ADDR : std_logic_vector(31 downto 0) := x"8000_0104";
  constant UART_BRD_ADDR : std_logic_vector(31 downto 0) := x"8000_0108";

begin
  clk <= not clk after 5 ns;
  rst <= '1', '0' after 50 ns;

  stim : process
    procedure reset_state is
    begin
      ud1_cs <= '0';
      ud1_addr <= (others => '0');
      ud1_wen <= (others => '0');
      ud1_din <= (others => '0');
      ud2_cs <= '0';
      ud2_addr <= (others => '0');
      ud2_wen <= (others => '0');
      ud2_din <= (others => '0');
    end reset_state;
  begin
    out_reg <= (others => '0');
    -- setup default values
    reset_state;

    wait until rst = '0';
    wait until rising_edge(clk);
    ud1_cs <= '1';
    ud1_addr <= UART_BRD_ADDR;
    ud1_din <= x"0000_0004";
    ud1_wen <= x"f";
    ud2_cs <= '1';
    ud2_addr <= UART_BRD_ADDR;
    ud2_din <= x"0000_0004";
    ud2_wen <= x"f";
    wait until (ud1_ack = '1') and (ud2_ack = '1');
    reset_state;
    wait until rising_edge(clk);

    ud1_cs <= '1';
    ud1_din <= x"0000_0012";
    ud1_addr <= UART_TX_ADDR;
    ud1_wen <= "0001";
    wait until ud1_ack = '1';
    ud1_cs <= '0';
    ud1_wen <= "0000";

    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);

    ud2_cs <= '1';
    ud2_addr <= UART_RX_ADDR;
    ud2_wen <= "0000";
    wait until ud2_ack = '1';
    wait until rising_edge(clk);
    ud2_cs <= '0';
    ud2_wen <= "0000";
    out_reg <= ud2_dout;

    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);

    if out_reg = x"0000_0012" then
      report "finished simulation successfully!";
    else
      report "simulation failed!" severity error;
    end if;

    stop;
  end process;

  ud1_inst : entity work.uart_device
  port map (
    clk   => clk,
    rst   => rst,
    cs    => ud1_cs,
    addr  => ud1_addr,
    wen   => ud1_wen,
    din   => ud1_din,
    dout  => ud1_dout,
    ack   => ud1_ack,

    txd => txd,
    rxd => rxd
  );

  ud2_inst : entity work.uart_device
  port map (
    clk   => clk,
    rst   => rst,
    cs    => ud2_cs,
    addr  => ud2_addr,
    wen   => ud2_wen,
    din   => ud2_din,
    dout  => ud2_dout,
    ack   => ud2_ack,

    txd => rxd,
    rxd => txd
  );
end behave;
