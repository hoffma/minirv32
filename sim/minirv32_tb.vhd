----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/03/2022 06:44:39 PM
-- Design Name: 
-- Module Name: minirv32_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library std;
use std.env.finish;

entity minirv32_tb is
--  Port ( );
end minirv32_tb;

architecture Behavioral of minirv32_tb is
    signal clk, rst : std_logic := '0';
    
    signal addr_i : integer := 0;
    
    signal LED : std_logic_vector(7 downto 0);
    
    type mem_array is array(natural range<>) of std_logic_vector(31 downto 0);
    -- constant instr_data : instr_array :=  (x"00200113", x"00400213", x"402202b3", x"ff5ff06f");
    
    constant MEM_SIZE_BYTES : integer := 16#10000#;
    
    signal ram_data : mem_array(0 to (MEM_SIZE_BYTES/4)-1) := (
    x"00010117",
    x"00010113",
    x"00000093",
    x"00000193",
    x"00000213",
    x"00000293",
    x"00000313",
    x"00000393",
    x"00000413",
    x"00000493",
    x"00000513",
    x"00000593",
    x"00000613",
    x"00000693",
    x"00000713",
    x"00000793",
    x"00000813",
    x"00000893",
    x"00000913",
    x"00000993",
    x"00000A13",
    x"00000A93",
    x"00000B13",
    x"00000B93",
    x"00000C13",
    x"00000C93",
    x"00000D13",
    x"00000D93",
    x"00000E13",
    x"00000E93",
    x"00000F13",
    x"00000F93",
    x"80000537",
    x"09900593",
    x"00B52023",
    x"05C000EF",
    x"0000006F",
    x"FF010113",
    x"00012623",
    x"00200713",
    x"00C12783",
    x"00F77663",
    x"01010113",
    x"00008067",
    x"00C12783",
    x"00178793",
    x"00F12623",
    x"FE5FF06F",
    x"00100793",
    x"02A7F063",
    x"00157793",
    x"00079663",
    x"00155513",
    x"00008067",
    x"00151793",
    x"00A78533",
    x"00150513",
    x"00008067",
    x"FF010113",
    x"00812423",
    x"90000437",
    x"00912223",
    x"01212023",
    x"00112623",
    x"00440413",
    x"00C00793",
    x"00F42023",
    x"00B00493",
    x"00100913",
    x"00042503",
    x"FA9FF0EF",
    x"00A42023",
    x"01250663",
    x"FFF48493",
    x"FE0496E3",
    x"00042703",
    x"900007B7",
    x"00E7A023",
    x"F5DFF0EF",
    x"FFDFF06F",
    others => (others => '0'));

    type cpu_interface is record
        addr : std_logic_vector(31 downto 0);
        wdata : std_logic_vector(31 downto 0);
        rdata : std_logic_vector(31 downto 0);
        wen : std_logic_vector(3 downto 0);
        strb : std_logic;
        ack : std_logic;
    end record cpu_interface;
    
    signal s_cpu : cpu_interface;
    
    signal tmp_sel : std_logic;
    signal tmp_reg : std_logic_vector(31 downto 0);
    signal fin_reg : std_logic_vector(31 downto 0);
begin

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 50 ns;
    
    tmp_sel <= '1' when s_cpu.addr(31 downto 16) = x"9000" else '0';

    mem_rw: process(clk)
        variable cnt : integer := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                cnt := 0;
                tmp_reg <= (others => '0');
                fin_reg <= (others => '0');
            end if;
            s_cpu.ack <= s_cpu.strb;
            s_cpu.rdata <= (others => '0');
            
            if s_cpu.addr(31 downto 16) = x"0000" then
                if s_cpu.wen(0) = '1' then
                    ram_data(addr_i)(7 downto 0) <= s_cpu.wdata(7 downto 0);
                end if;
                if s_cpu.wen(1) = '1' then
                    ram_data(addr_i)(15 downto 8) <= s_cpu.wdata(15 downto 8);
                end if;
                if s_cpu.wen(2) = '1' then
                    ram_data(addr_i)(23 downto 16) <= s_cpu.wdata(23 downto 16);
                end if;
                if s_cpu.wen(3) = '1' then
                    ram_data(addr_i)(31 downto 24) <= s_cpu.wdata(31 downto 24);
                end if;
                s_cpu.rdata <= ram_data(addr_i); 
            elsif s_cpu.addr(31 downto 16) = x"9000" then
                if s_cpu.addr(7 downto 0) = x"04" then
                    if s_cpu.wen /= "0000" then
                        cnt := cnt + 1;
                        tmp_reg <= s_cpu.wdata;
                        report "Got tmp_reg data: 0x" & to_hstring(s_cpu.wdata) severity note;
                    else
                        s_cpu.rdata <= tmp_reg;
                    end if;
                    if cnt > 20 then
                        report "Simulation not halting in time. Aborting" severity failure;
                        finish(1);
                    end if;
                elsif s_cpu.addr(7 downto 0) = x"00" then
                    if s_cpu.wdata = x"0000_0001" then
                        report "Simulation finished successfully";
                        finish(0);
                    else
                        report "Simulation failed!" severity failure;
                        finish(1);
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    -- elsif mem_addr(31 downto 24) = x"02" and mem_wen(0) = '1' then
    -- LED <= mem_wdat(7 downto 0);
    
    addr_i <= to_integer(unsigned(s_cpu.addr(23 downto 2)));
    
    inst_proc : entity work.minirv32 
    port map (
        clk => clk,
        rst => rst,
        mem_addr => s_cpu.addr,
        mem_wdata => s_cpu.wdata,
        mem_wen => s_cpu.wen,
        mem_rdata => s_cpu.rdata,
        mem_strb => s_cpu.strb,
        mem_ack => s_cpu.ack
    );
    
end Behavioral;
