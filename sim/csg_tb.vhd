library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library std;
use std.env.stop;

entity csg_tb is
end csg_tb;

architecture Behavioral of csg_tb is
    signal clk : std_logic := '0';
    signal addr : std_logic_vector(31 downto 0) := (others => '0');
    signal cs_lines_o : std_logic_vector(7 downto 0) := (others => '0');
begin
    clk <= not clk after 5 ns;
    
    stim : process
    begin
        addr <= x"0000_1234";
        wait until rising_edge(clk);
        addr <= x"0000_7abc";
        wait until rising_edge(clk);
        addr <= x"8000_0000";
        wait until rising_edge(clk);
        addr <= x"8000_0004";
        wait until rising_edge(clk);
        addr <= x"8000_0100";
        wait until rising_edge(clk);
        addr <= x"8000_0110";
        wait until rising_edge(clk);
        addr <= x"a000_0000";
        wait until rising_edge(clk);
        addr <= x"b010_0020";
        
        report "finished simulation";
        stop;
        
    end process;

    inst_csg : entity work.csg
    port map (
        addr => addr,
        cs_lines => cs_lines_o
    );

end Behavioral;
