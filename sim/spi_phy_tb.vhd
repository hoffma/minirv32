library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.stop;

entity spi_phy_tb is
end spi_phy_tb;

architecture behave of spi_phy_tb is
  signal clk : std_logic := '0';
  signal strb, we : std_logic := '0';
  signal din : std_logic_vector(7 downto 0);
  -- signal dout : std_logic_vector(7 downto 0);
  signal ack : std_logic;

  signal sclk : std_logic;
  signal ncs : std_logic;
  signal mosi : std_logic;
  -- signal miso : std_logic;

  constant DISPLAY_TEST_REG : std_logic_vector(7 downto 0) := x"0f";
  constant DISPLAY_TEST_EN  : std_logic_vector(7 downto 0) := x"01";
  constant DISPLAY_TEST_nEN : std_logic_vector(7 downto 0) := x"00";
begin

  clk <= not clk after 20 ns;

  stim : process
  begin
    we <= '0';
    strb <= '0';
    din <= (others => '0');
    wait for 100 ns;
    wait until rising_edge(clk);
    strb <= '1';
    din <= DISPLAY_TEST_REG;
    wait until ack = '1';
    din <= DISPLAY_TEST_EN;
    wait until rising_edge(clk);
    strb <= '0';
    wait until ack = '1';
    wait for 2 us;
    report "Simulation finished" severity NOTE;
    stop;
  end process;

  inst_spi_phy : entity work.spi_phy
  generic map (
    CLK_IN_HZ => 25e6,
    SCLK_OUT_HZ => 1e6
  )
  port map (
    clk => clk,
    strb => strb,
    we => we,
    din => din,
    -- dout => dout,
    ack => ack,

    sclk => sclk,
    ncs => ncs,
    mosi => mosi
    -- miso => miso
  );
end behave;
