library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.env.stop;

entity spi_device_tb is
end spi_device_tb;

architecture behave of spi_device_tb is
  signal clk, rst : std_logic := '0';
  signal cs : std_logic;
  signal addr : std_logic_vector(31 downto 0);
  signal wen : std_logic_vector(3 downto 0);
  signal din, dout : std_logic_vector(31 downto 0);
  signal ack : std_logic;

  signal sclk, ncs, mosi : std_logic;
  signal busy : std_logic;

  constant DISPLAY_TEST_REG : std_logic_vector(7 downto 0) := x"0f";
  constant DISPLAY_TEST_EN  : std_logic_vector(7 downto 0) := x"01";
  constant DISPLAY_TEST_nEN : std_logic_vector(7 downto 0) := x"00";

  constant DISPLAY_ADDR_TX : std_logic_vector(31 downto 0) := x"80000200";
  constant DISPLAY_ADDR_RX : std_logic_vector(31 downto 0) := x"80000204";
  constant DISPLAY_ADDR_BUSY : std_logic_vector(31 downto 0) := x"80000208";

  signal tmp_din : std_logic_vector(31 downto 0);
begin

  clk <= not clk after 20 ns;
  rst <= '1', '0' after 200 ns;

  stim : process
    procedure is_busy(signal busy_o : out std_logic) is
    begin
      cs <= '1';
      addr <= DISPLAY_ADDR_BUSY;
      wait until ack = '1';
      wait until rising_edge(clk);
      cs <= '0';
      busy_o <= dout(0);
      wait until rising_edge(clk);
    end procedure is_busy;

    procedure write_spi(signal addr_i : in std_logic_vector(31 downto 0);
                       signal data_i : in std_logic_vector(31 downto 0)) is
    begin
      addr <= addr_i;
      din <= data_i;
      wen <= x"f";
      cs <= '1';
      wait until ack = '1';
      cs <= '0';
      wen <= x"0";
      wait until rising_edge(clk);
    end procedure write_spi;

  begin
    busy <= '0';
    cs <= '0';
    addr <= (others => '0');
    wen <= (others => '0');
    din <= (others => '0');
    wait until rst = '0';
    wait until rising_edge(clk);

    loop
      is_busy(busy);
      exit when busy = '0';
      wait until rising_edge(clk);
    end loop;

    wait until rising_edge(clk);
    addr <= DISPLAY_ADDR_TX;
    din <= x"0000" & DISPLAY_TEST_EN & DISPLAY_TEST_REG;
    wen <= x"f";
    cs <= '1';
    wait until ack = '1';
    cs <= '0';
    wen <= x"0";
    wait until rising_edge(clk);

    loop
      is_busy(busy);
      report "busy: " &  std_logic'image(busy); -- & busy;
      exit when busy = '0';
      wait until rising_edge(clk);
    end loop;

    wait until rising_edge(clk);
    addr <= DISPLAY_ADDR_TX;
    din <= x"0000" & DISPLAY_TEST_nEN & DISPLAY_TEST_REG;
    wen <= x"f";
    cs <= '1';
    wait until ack = '1';
    cs <= '0';
    wen <= x"0";
    wait until rising_edge(clk);

    loop
      is_busy(busy);
      report "busy: " &  std_logic'image(busy); -- & busy;
      exit when busy = '0';
      wait until rising_edge(clk);
    end loop;

    wait for 2 us;
    report "finished simulation" severity NOTE;
    stop;
  end process;

  inst_spi_device : entity work.spi_device
  generic map (
    CLK_IN_HZ => 25e6,
    SCLK_OUT_HZ => 1e6
  )
  port map (
    clk => clk,
    rst => rst,
    cs => cs,
    addr => addr,
    wen => wen,
    din => din,
    dout => dout,
    ack => ack,

    sclk => sclk,
    ncs => ncs,
    mosi => mosi
  );
end behave;
